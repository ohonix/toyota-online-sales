import { useTheme } from '@emotion/react'
import { Box, Container, Flex } from '@grid'

const items = [
  {
    name: 'Vios',
  },
  {
    name: 'Avanza',
  },
  {
    name: 'Sienta',
  },
]
export default function CustomizeNavCars() {
  const { variables } = useTheme()
  return (
    <div
      css={{
        backgroundColor: '#F5F5F7',
        boxShadow: '0 .3rem .6rem rgba(0, 0, 0, 0.16)',
      }}>
      <Container>
        <div
          css={{
            marginLeft: '-1.4rem',
            marginRight: '-1.4rem',
          }}>
          <Flex>
            {items.map((item, i) => {
              const isActive = i === 0
              return (
                <Box key={i}>
                  <a
                    css={{
                      display: 'inline-flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      padding: '0.5rem 1rem',
                      margin: '0 .4rem',
                      height: '4.4rem',
                      position: 'relative',
                      fontSize: '2.4rem',
                      color: '#6A6B70',
                      ...(isActive && {
                        color: '#000',
                        fontFamily: 'db_helvethaica_x65_med',
                      }),
                      '&:after': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        height: '.3rem',
                        backgroundColor: variables.colors.highlight.lvl1,
                        opacity: 0,
                        width: 0,
                        transition: 'all 0.3s',
                        ...(isActive && {
                          opacity: 1,
                          width: '100%',
                        }),
                      },
                    }}>
                    {item.name}
                  </a>
                </Box>
              )
            })}
          </Flex>
        </div>
      </Container>
    </div>
  )
}
