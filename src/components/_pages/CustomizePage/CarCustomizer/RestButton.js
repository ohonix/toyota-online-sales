import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

export default function ResetButton({ onClick = () => null }) {
  return (
    <div
      onClick={onClick}
      css={{
        marginLeft: 'auto',
        display: 'inline-flex',
        alignItems: 'center',
        cursor: 'pointer',
        color: '#6A6B70',
        fontSize: '2rem',
        [media('lg')]: {
          fontSize: '2.4rem',
        },
        [media('xl')]: {
          '&:hover': {
            color: '#ED1B2F',
          },
        },
      }}>
      <img
        css={{
          width: '1.4rem',
          height: '1.4rem',
          marginRight: '1rem',
          display: 'block',
        }}
        src={getStatic('images/customize/icon-refresh.svg')}
      />
      <div>รีเซ็ท</div>
    </div>
  )
}
