import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

const imagePath = 'images/customize/accessories-selection/'
export const carAccessoriesItems = [
  {
    label: 'ชุดแต่งรอบคัน ของ Fuastino',
    price: 50000,
    priceDiscount: 32000,
    link: '',
    key: 'accessories-a',
    car: {
      image: '',
      image2x: '',
    },
    dot: {
      image: imagePath + 'accessories-a.png',
      image2x: imagePath + 'accessories-a@2x.png',
    },
  },
  {
    label: 'ชุดแต่งรอบคัน ของ EJ5NrSvFc',
    price: 20000,
    link: '',
    key: 'accessories-b',
    car: {
      image: '',
      image2x: '',
    },
    dot: {
      image: imagePath + 'accessories-b.png',
      image2x: imagePath + 'accessories-b@2x.png',
    },
  },
  {
    label: 'ชุดแต่งรอบคัน ของ NkHSrHvYq',
    price: 30000,
    priceDiscount: 22000,
    link: '',
    key: 'accessories-c',
    car: {
      image: '',
      image2x: '',
    },
    dot: {
      image: imagePath + 'accessories-c.png',
      image2x: imagePath + 'accessories-c@2x.png',
    },
  },
]

export default function CarAccessoriesSelection({
  selectedAccessorieSetIndex,
  setSelectedAccessorieSetIndex,
}) {
  function selectCarAccessories(i) {
    return event => {
      setSelectedAccessorieSetIndex(selectedAccessorieSetIndex => {
        if (selectedAccessorieSetIndex === i) {
          return undefined
        }
        return i
      })
    }
  }

  const isSelected = selectedAccessorieSetIndex !== undefined

  const currentCarColor = isSelected
    ? carAccessoriesItems[selectedAccessorieSetIndex]
    : undefined
  return (
    <>
      <div>
        <div
          css={{
            overflowY: 'hidden',
            overflowX: 'auto',
            textAlign: 'center',
            marginBottom: '1rem',
          }}>
          <div
            css={{
              display: 'inline-flex',
              overflowY: 'hidden',
              overflowX: 'auto',
              padding: '2rem 1rem .2rem',
            }}>
            {carAccessoriesItems.map((item, i) => {
              const isActive = selectedAccessorieSetIndex === i
              return (
                <div
                  key={i}
                  onClick={selectCarAccessories(i)}
                  css={{
                    position: 'relative',
                    marginRight: '1rem',
                    marginLeft: '1rem',
                    borderRadius: '.4rem',
                    border: '1px solid #9B9B9B',
                    marginBottom: '2rem',
                    cursor: 'pointer',
                    [media('lg')]: {
                      marginRight: '1.5rem',
                      marginLeft: '1.5rem',
                    },
                  }}>
                  <Image
                    css={{
                      borderRadius: '.4rem',
                      width: '8rem',
                      height: 'auto',
                      maxWidth: 'none',
                      [media('lg')]: {
                        width: '17rem',
                      },
                    }}
                    src={getStatic(item.dot.image)}
                    srcSet={getStatic(item.dot.image2x)}
                  />
                  <img
                    css={{
                      display: 'block',
                      width: '2.6rem',
                      height: '2.6rem',
                      zIndex: 2,
                      pointerEvents: 'none',
                      position: 'absolute',
                      top: '-1.3rem',
                      right: '-1.3rem',
                      maxWidth: 'none',
                      opacity: 0,
                      transform: 'scale(1.2)',
                      transition: 'all 0.5s',
                      ...(isActive && {
                        opacity: 1,
                        transform: 'scale(1)',
                      }),
                    }}
                    src={getStatic('images/customize/icon-checked.svg')}
                  />
                  <div
                    css={{
                      borderRadius: '.4rem',
                      position: 'absolute',
                      left: '-1px',
                      top: '-1px',
                      right: '-1px',
                      bottom: '-1px',
                      border: '2px solid transparent',
                      transition: 'all 0.5s',
                      ...(isActive && {
                        border: '2px solid #ED1B2F',
                      }),
                    }}>
                    <img
                      css={{
                        position: 'absolute',
                        left: '-2px',
                        bottom: '-2px',
                        height: '1.2rem',
                        width: 'auto',
                        transition: 'all 0.5s',
                        transform: 'translateX(1rem)',
                        opacity: 0,
                        [media('lg')]: {
                          height: '2.5rem',
                        },
                        ...(isActive &&
                          isSelected &&
                          currentCarColor.priceDiscount && {
                            opacity: 1,
                            transform: 'translateX(0)',
                          }),
                      }}
                      src={getStatic('images/customize/label-promotion.svg')}
                    />
                  </div>
                </div>
              )
            })}
          </div>
        </div>
        {isSelected && (
          <div
            css={{
              textAlign: 'center',
              lineHeight: 1,
            }}>
            <div
              css={{
                fontSize: '2.2rem',
                color: '#000000',
                [media('lg')]: { fontSize: '2.4rem' },
              }}>
              {currentCarColor.label}
            </div>
            <div
              css={{
                fontSize: '2.2rem',
                color: '#ED1B2F',
                fontFamily: 'db_helvethaica_x75_bd',
              }}>
              {currentCarColor.price.toLocaleString()}
              {currentCarColor.priceDiscount && (
                <span
                  css={{
                    fontSize: '1.8rem',
                    color: '#9B9B9B',
                    textDecoration: 'line-through',
                    marginLeft: '1rem',
                  }}>
                  {currentCarColor.priceDiscount.toLocaleString()}
                </span>
              )}
            </div>
            <div
              css={{
                fontSize: '1.8rem',
                color: '#6A6B70',
                textDecoration: 'underline',
              }}>
              ดูรายละเอียด
            </div>
          </div>
        )}
      </div>
    </>
  )
}
