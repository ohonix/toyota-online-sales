import { media } from '@lib/styles/helpers'
import { carAccessoriesItems } from './CarAccessoriesSelection'
export default function CarPriceDisplay({ selectedAccessorieSetIndex }) {
  let price = 789000
  if (selectedAccessorieSetIndex !== undefined) {
    price = price + carAccessoriesItems[selectedAccessorieSetIndex].price
  }

  return (
    <div
      css={{
        lineHeight: 1,
        fontSize: '2.4rem',
        color: '#000',
        [media('lg')]: {
          fontSize: '3.2rem',
          marginBottom: '1rem',
        },
      }}>
      ราคา{' '}
      <span
        css={{
          fontSize: '3.6rem',
          [media('lg')]: {
            fontSize: '4.8rem',
          },
        }}>
        {price.toLocaleString()}
      </span>{' '}
      บาท
    </div>
  )
}
