import { useState } from 'react'
import { Tab, Tabs, TabList, TabPanel, resetIdCounter } from 'react-tabs'
import { Container } from '@grid'
import Button from '@components/_common/Button'
import { media, Responsive } from '@lib/styles/helpers'
import CarModelSelection from './CarModelSelection'
import CarColorSelection, { carColorItems } from './CarColorSelection'
import CarAccessoriesSelection, {
  carAccessoriesItems,
} from './CarAccessoriesSelection'
import SingleCarDisplay from './SingleCarDisplay'

import CarPriceDisplay from './CarPriceDisplay'
import ResetButton from './RestButton'
resetIdCounter()

export default function CarCustomizer() {
  const [selectedColorIndex, setSelectedColorIndex] = useState(0)
  const [selectedAccessorieSetIndex, setSelectedAccessorieSetIndex] = useState()
  function resetSelected() {
    setSelectedColorIndex(0)
    setSelectedAccessorieSetIndex()
  }
  return (
    <div
      css={{
        paddingBottom: '5rem',
      }}>
      <Container>
        <div
          css={{
            paddingTop: '1rem',
            [media('lg')]: {
              paddingTop: '3rem',
              display: 'flex',
              alignItems: 'center',
            },
          }}>
          <div>
            <div
              css={{
                display: 'flex',
                alignItems: 'center',
              }}>
              <div
                css={{
                  fontSize: '4.8rem',
                  lineHeight: 1,
                  color: '#000',
                  marginRight: '1.5rem',
                  [media('lg')]: {
                    fontSize: '6rem',
                  },
                }}>
                Vios
              </div>
              <div
                css={{
                  width: '24rem',
                  paddingLeft: '1.5rem',
                  position: 'relative',
                  '&:after': {
                    content: '""',
                    display: 'block',
                    height: '1.5rem',
                    width: '1px',
                    position: 'absolute',
                    left: 0,
                    top: '50%',
                    transform: 'translateY(-50%)',
                    backgroundColor: '#000',
                  },
                }}>
                <CarModelSelection
                  selectedAccessorieSetIndex={selectedAccessorieSetIndex}
                />
              </div>
            </div>
            <div
              css={{
                color: '#86868B',
                fontSize: '2.4rem',
                [media('lg')]: {
                  fontSize: '3.2rem',
                },
              }}>
              All is Possible
            </div>
          </div>
          <div
            css={{
              display: 'flex',
              width: '100%',
              alignItems: 'center',
              [media('lg')]: {
                display: 'block',
                width: 'auto',
                textAlign: 'right',
                marginLeft: 'auto',
              },
            }}>
            <CarPriceDisplay
              selectedAccessorieSetIndex={selectedAccessorieSetIndex}
            />
            <Responsive
              desktop={
                <div>
                  <Button css={{ minWidth: '17rem' }}>จองเลย</Button>
                </div>
              }
              mobile={<ResetButton onClick={resetSelected} />}
            />
          </div>
        </div>
        <SingleCarDisplay
          carColorIndex={selectedColorIndex}
          carAccessoriesIndex={selectedAccessorieSetIndex}
          carColorItems={carColorItems}
          setSelectedColorIndex={setSelectedColorIndex}
        />
        <div
          css={{
            '.react-tabs__tab-list': {
              display: 'flex',
            },
            '.react-tabs__tab': {
              display: 'block',
              fontSize: '2.4rem',
              padding: '0 1.5rem 1.5rem',
              position: 'relative',
              cursor: 'pointer',
              [media('xl')]: {
                '&:hover': {
                  color: '#ED1B2F',
                },
              },
              '&:after': {
                content: '""',
                display: 'block',
                height: '.3rem',
                backgroundColor: '#ED1B2F',
                position: 'absolute',
                left: 0,
                bottom: 0,
                width: 0,
                transition: 'all 0.3s',
              },
              '&.react-tabs__tab--selected': {
                '&:after': {
                  width: '100%',
                },
              },
            },
          }}>
          <div
            css={{
              marginBottom: '3rem',
              [media('lg')]: {
                textAlign: 'center',
                position: 'relative',
              },
            }}>
            <Responsive
              desktop={
                <div
                  css={{
                    position: 'absolute',
                    right: 0,
                    top: '1.5rem',
                  }}>
                  <ResetButton onClick={resetSelected} />
                </div>
              }
            />
            <Tabs>
              <div
                css={{
                  padding: '0 1rem',
                  [media('lg')]: {
                    display: 'inline-flex',
                    minWidth: '53rem',
                    margin: 'auto',
                  },
                }}>
                <TabList>
                  <Tab>สี</Tab>
                  <Tab>
                    ชุดแต่ง
                    {selectedAccessorieSetIndex !== undefined && (
                      <div
                        css={{
                          width: '.4rem',
                          height: '.4rem',
                          borderRadius: '100%',
                          backgroundColor: '#ED1B2F',
                          position: 'absolute',
                          right: '.5rem',
                          top: '.5rem',
                        }}
                      />
                    )}
                  </Tab>
                </TabList>
              </div>
              <TabPanel>
                <div
                  css={{
                    borderRadius: '.8rem',
                    backgroundColor: '#F2F2F2',
                    padding: '1rem 0rem 3rem',
                  }}>
                  <CarColorSelection
                    selectedColorIndex={selectedColorIndex}
                    setSelectedColorIndex={setSelectedColorIndex}
                  />
                </div>
              </TabPanel>
              <TabPanel>
                <div
                  css={{
                    borderRadius: '.8rem',
                    backgroundColor: '#F2F2F2',
                    padding: '1rem 0rem 3rem',
                  }}>
                  <CarAccessoriesSelection
                    selectedAccessorieSetIndex={selectedAccessorieSetIndex}
                    setSelectedAccessorieSetIndex={
                      setSelectedAccessorieSetIndex
                    }
                  />
                </div>
              </TabPanel>
            </Tabs>
          </div>
          <Responsive
            mobile={
              <div
                css={{
                  textAlign: 'center',
                }}>
                <Button css={{ minWidth: '16.3rem' }}>จองเลย</Button>
              </div>
            }
          />
        </div>
      </Container>
    </div>
  )
}
