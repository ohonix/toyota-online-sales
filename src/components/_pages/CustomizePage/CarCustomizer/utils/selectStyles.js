import { media } from '@lib/styles/helpers'

export function setSelectStylesControl(provided, state) {
  return {
    ...provided,
    border: 'none',
    boxShadow: 'none',
    minHeight: '2.8rem',
    cursor: 'pointer',
  }
}

export function setSelectSingleValue(provided, state) {
  return {
    ...provided,
    fontSize: '2.4rem',
    margin: '0',
    lineHeight: 1,
    [media('lg')]: {
      fontSize: '3.2rem',
    },
  }
}

export function setSelectStylesIndicatorSeparator(provided, state) {
  return {
    display: 'none',
  }
}
export function setSelectStylesIndicatorsContainer(provided, state) {
  return {
    ...provided,
    svg: {
      fill: '#000',
    },
  }
}

export function setSelectStylesOption(provided, { isSelected, isFocused }) {
  return {
    ...provided,
    color: isSelected ? '#ED1B2F' : isFocused ? '#fff' : '#6A6B70',
    background: (isFocused && !isSelected ? '#ED1B2F' : 'none') + ' !important',
    padding: '1rem 2rem',
    cursor: 'pointer',
  }
}

export function setValueContainer(provided) {
  return {
    ...provided,
    padding: '0 1rem',
  }
}
