import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'

const imagePath = 'images/customize/color-selection/'
export const carColorItems = [
  {
    label: 'Red Mica Metallic',
    key: 'red-mica-metallic',
    car: {
      image: imagePath + 'car-red-mica-metallic.png',
      image2x: imagePath + 'car-red-mica-metallic@2x.png',
    },
    dot: {
      image: imagePath + 'color-red-mica-metallic.png',
      image2x: imagePath + 'color-red-mica-metallic@2x.png',
    },
  },
  {
    label: 'Quartz Brown Metallic',
    key: 'quartz-brown-metallic',
    car: {
      image: imagePath + 'car-quartz-brown-metallic.png',
      image2x: imagePath + 'car-quartz-brown-metallic@2x.png',
    },
    dot: {
      image: imagePath + 'color-quartz-brown-metallic.png',
      image2x: imagePath + 'color-quartz-brown-metallic@2x.png',
    },
  },
  {
    label: 'Gray Metallic',
    key: 'gray-metallic',
    car: {
      image: imagePath + 'car-gray-metallic.png',
      image2x: imagePath + 'car-gray-metallic@2x.png',
    },
    dot: {
      image: imagePath + 'color-gray-metallic.png',
      image2x: imagePath + 'color-gray-metallic@2x.png',
    },
  },
  {
    label: 'Attitude Black Mica',
    key: 'attitude-black-mica',
    car: {
      image: imagePath + 'car-attitude-black-mica.png',
      image2x: imagePath + 'car-attitude-black-mica@2x.png',
    },
    dot: {
      image: imagePath + 'color-attitude-black-mica.png',
      image2x: imagePath + 'color-attitude-black-mica@2x.png',
    },
  },
  {
    label: 'Silver Metallic',
    key: 'silver-metallic',
    car: {
      image: imagePath + 'car-silver-metallic.png',
      image2x: imagePath + 'car-silver-metallic@2x.png',
    },
    dot: {
      image: imagePath + 'color-silver-metallic.png',
      image2x: imagePath + 'color-silver-metallic@2x.png',
    },
  },
  {
    label: 'Super White',
    key: 'super-white',
    car: {
      image: imagePath + 'car-super-white.png',
      image2x: imagePath + 'car-super-white@2x.png',
    },
    dot: {
      image: imagePath + 'color-super-white.png',
      image2x: imagePath + 'color-super-white@2x.png',
    },
  },
]

export default function CarColorSelection({
  selectedColorIndex,
  setSelectedColorIndex,
}) {
  function selectCarColor(i) {
    return event => {
      setSelectedColorIndex(i)
    }
  }

  const currentCarColor = carColorItems[selectedColorIndex]
  return (
    <>
      <div>
        <div
          css={{
            overflowY: 'hidden',
            overflowX: 'auto',
            textAlign: 'center',
            marginBottom: '3rem',
          }}>
          <div
            css={{
              display: 'inline-flex',
              padding: '2rem 2.5rem .2rem',
            }}>
            {carColorItems.map((item, i) => {
              const isActive = selectedColorIndex === i
              return (
                <div
                  key={i}
                  onClick={selectCarColor(i)}
                  css={{
                    position: 'relative',
                    marginRight: '1rem',
                    marginLeft: '1rem',
                    marginBottom: '2rem',
                    cursor: 'pointer',
                  }}>
                  <Image
                    css={{
                      width: '4.6rem',
                      height: 'auto',
                      maxWidth: 'none',
                    }}
                    src={getStatic(item.dot.image)}
                    srcSet={getStatic(item.dot.image2x)}
                  />
                  <img
                    css={{
                      display: 'block',
                      width: '5.5rem',
                      height: 'auto',
                      zIndex: 2,
                      pointerEvents: 'none',
                      position: 'absolute',
                      left: '-.3rem',
                      bottom: '-.3rem',
                      maxWidth: 'none',
                      opacity: 0,
                      transform: 'scale(1.2)',
                      transition: 'all 0.5s',
                      ...(isActive && {
                        opacity: 1,
                        transform: 'scale(1)',
                      }),
                    }}
                    src={getStatic('images/customize/icon-color-selected.svg')}
                  />
                </div>
              )
            })}
          </div>
        </div>
        <div
          css={{ fontSize: '2.2rem', color: '#000000', textAlign: 'center' }}>
          {currentCarColor.label}
        </div>
      </div>
    </>
  )
}
