import {
  setSelectStylesControl,
  setSelectSingleValue,
  setSelectStylesIndicatorsContainer,
  setSelectStylesIndicatorSeparator,
  setSelectStylesOption,
  setValueContainer,
} from './utils/selectStyles'
import Select from 'react-select'

export default function CarModelSelection() {
  const options = [
    { value: 'entry-grade', label: 'Entry Grade' },
    { value: 'mid-grade', label: 'Mid Grade' },
    { value: 'high-grade', label: 'High Grade' },
  ]
  return (
    <Select
      instanceId="4kAQDbPK5"
      options={options}
      defaultValue={options[2]}
      styles={{
        control: setSelectStylesControl,
        singleValue: setSelectSingleValue,
        indicatorSeparator: setSelectStylesIndicatorSeparator,
        indicatorsContainer: setSelectStylesIndicatorsContainer,
        option: setSelectStylesOption,
        valueContainer: setValueContainer,
      }}
    />
  )
}
