import { useRef, useEffect } from 'react'
import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { EffectFade } from 'swiper'
import 'swiper/swiper.min.css'
import 'swiper/components/effect-fade/effect-fade.min.css'

SwiperCore.use([EffectFade])

export default function SingleCarDisplay({
  carColorItems,
  carColorIndex,
  carAccessoriesIndex,
  setSelectedColorIndex,
}) {
  const refSwipter = useRef()

  useEffect(() => {
    if (refSwipter.current !== undefined) {
      refSwipter.current.slideTo(carColorIndex)
    }
  }, [carColorIndex])

  return (
    <div
      css={{
        pointerEvents: 'none',
        [media('lg')]: {
          marginTop: '-15rem',
        },
      }}>
      <Swiper
        effect="fade"
        onSlideChange={({ activeIndex }) => {
          if (activeIndex !== carColorIndex) {
            setSelectedColorIndex(activeIndex)
          }
        }}
        onSwiper={swiper => (refSwipter.current = swiper)}>
        {carColorItems.map((carColorItem, i) => {
          return (
            <SwiperSlide key={carColorItem.key}>
              <Image
                css={{
                  width: '35.2rem',
                  height: 'auto',
                  margin: 'auto',
                  [media('lg')]: {
                    width: '73rem',
                  },
                }}
                src={getStatic(carColorItem.car.image)}
                srcSet={getStatic(carColorItem.car.image2x)}
              />
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}
