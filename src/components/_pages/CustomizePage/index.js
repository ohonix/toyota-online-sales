import { Page } from '@lib/page'
import * as metaConfig from './meta'
import CustomizeNavCars from './CustomizeNavCars'
import CarCustomizer from './CarCustomizer'

export default function CustomizePage() {
  return (
    <Page metaConfig={metaConfig}>
      <CustomizeNavCars />
      <CarCustomizer />
    </Page>
  )
}
