import { Page } from '@lib/page'
import { Box, Container, Flex } from '@grid'
import * as metaConfig from './meta'

import Breadcrumb from '@components/_common/Breadcrumb'
import Thumbnail from '@components/_pages/HomePage/Privilege/Thumbnail'
import { getStatic } from '@lib/static'

const breadcrumbItems = [
  {
    label: 'หน้าหลัก',
    route: 'home',
  },
  {
    label: 'สิทธิประโยชน์อีกมากมาย',
  },
]

const items = [1, 2, 3, 4, 5, 6, 7]

export default function PrivilegeListPage() {
  return (
    <Page metaConfig={metaConfig}>
      <Container>
        <Breadcrumb items={breadcrumbItems} />
        <h1
          css={{
            fontSize: '3rem',
            color: '#000000',
            marginBottom: '3rem',
          }}>
          สิทธิประโยชน์อีกมากมาย
        </h1>
        <div
          css={{
            margin: '0 -1rem 2rem',
          }}>
          <Flex>
            {items.map(item => (
              <Box
                key={item}
                width={[1, 1, 1 / 2, 1 / 3]}
                css={{
                  paddingLeft: '1rem',
                  paddingRight: '1rem',
                }}>
                <Thumbnail isLightTheme />
              </Box>
            ))}
          </Flex>
        </div>
        <div
          css={{
            textAlign: 'center',
            padding: '0 0 5rem',
          }}>
          <div
            css={{
              display: 'inline-flex',
              alignItems: 'center',
              cursor: 'pointer',
              padding: '1rem',
            }}>
            <div
              css={{
                color: '#D70000',
                marginRight: '.7rem',
              }}>
              ดูสิทธิประโยชน์เพิ่มเติม
            </div>
            <div>
              <img
                css={{
                  display: 'block',
                  width: '.9rem',
                  height: 'auto',
                }}
                src={getStatic('images/privilege/icon-chevron-down.svg')}
              />
            </div>
          </div>
        </div>
      </Container>
    </Page>
  )
}
