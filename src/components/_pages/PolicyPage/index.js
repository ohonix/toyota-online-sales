import Breadcrumb from '@components/_common/Breadcrumb'
import { Container } from '@grid'
import { Page } from '@lib/page'
import * as metaConfig from './meta'

import contentPolicy from './contentPolicy'

import { media } from '@lib/styles/helpers'
import CustomSelect from '@components/_common/CustomSelect'

const breadcrumbItems = [
  {
    label: 'หน้าหลัก',
    route: 'home',
  },
  {
    label: 'เงื่อนไขและนโยบายต่างๆ',
  },
]

const selectOptions = [
  { value: '000', label: 'เงื่อนไขการใช้งาน' },
  { value: 'aaa', label: 'Lorem, ipsum.' },
  { value: 'bbb', label: 'Saepe, ipsa.' },
  { value: 'ccc', label: 'Possimus, neque.' },
]

export default function PolicyPage() {
  return (
    <Page metaConfig={metaConfig}>
      <Container>
        <Breadcrumb items={breadcrumbItems} />
        <div
          css={{
            [media('lg')]: {
              display: 'flex',
            },
          }}>
          <h1
            css={{
              color: '#000000',
              fontSize: '3rem',
              fontWeight: 'normal',
              fontFamily: 'db_helvethaica_x65_med',
              marginBottom: '2rem',
              [media('lg')]: {
                paddingRight: '2rem',
              },
              [media('xl')]: {
                fontSize: '4.8rem',
              },
            }}>
            เงื่อนไขและนโยบายต่างๆ
          </h1>
          <div
            css={{
              marginBottom: '3rem',
              [media('sm')]: {
                width: '26.7rem',
              },
              [media('lg')]: {
                marginLeft: 'auto',
                marginBottom: 0,
              },
            }}>
            <CustomSelect
              instanceId="EJK92EnY9"
              options={selectOptions}
              defaultValue={selectOptions[0]}
            />
          </div>
        </div>
        <div
          css={{
            fontSize: '1.8rem',
            color: '#000000',
            [media('xl')]: {
              fontSize: '2rem',
            },
            p: {
              marginBottom: '2rem',
            },
            a: {
              color: '#86868B',
              textDecoration: 'underline',
              '&:hover': {
                color: '#ED1B2F',
              },
            },
          }}
          dangerouslySetInnerHTML={{ __html: contentPolicy }}
        />
      </Container>
    </Page>
  )
}
