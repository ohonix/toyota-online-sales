import * as yup from 'yup'

export default yup.object().shape({
  name: yup
    .string()
    .trim()
    .required('กรุณากรอกชื่อ'),
  lastname: yup
    .string()
    .trim()
    .required('กรุณากรอกนามสกุล'),
  email: yup
    .string()
    .trim()
    .email('อีเมลไม่ถูกต้อง')
    .required('กรุณากรอกอีเมล'),
  telephone: yup
    .string()
    .trim()
    .required('กรุณากรอกเบอร์โทรศัพท์'),
  title: yup
    .string()
    .trim()
    .required('กรุณาเลือกหัวข้อ'),
  detail: yup
    .string()
    .trim()
    .required('กรุณากรอกรายละเอียด'),
})
