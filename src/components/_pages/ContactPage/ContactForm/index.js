import { useState } from 'react'
import Form from '@lib/form/Form'

import Template from './Template'
import schema from './schema'
import { media } from '@lib/styles/helpers'
import ModalMessage from '@components/_common/ModalMessage'
import { getStatic } from '@lib/static'

export default function ContactForm({ defaultValues }) {
  const [modalIsOpen, setIsOpen] = useState(false)
  function openModal() {
    setIsOpen(true)
  }
  function closeModal() {
    setIsOpen(false)
  }
  function onSubmit(data) {
    openModal(openModal)
  }
  return (
    <>
      <div
        css={{
          padding: '2rem 0 6rem',
          borderTop: '1px solid #BCBDC1',
          [media('lg')]: {
            padding: '0',
            borderTop: 'none',
          },
        }}>
        <div
          css={{
            color: '#000000',
            fontSize: '3rem',
          }}>
          โตโยต้า พร้อมให้บริการคุณ
        </div>
        <div
          css={{
            color: '2.2rem',
            fontSize: '#86868B',
            marginBottom: '2rem',
          }}>
          หากคุณมีคำแนะนำ ข้อคิดเห็น หรือข้อสงสัยเกี่ยวกับ
          รถยนต์และบริการของโตโยต้า
        </div>
        <Form defaultValues={defaultValues} schema={schema} onSubmit={onSubmit}>
          {Template}
        </Form>
      </div>
      <ModalMessage isOpen={modalIsOpen} closeModal={closeModal}>
        <div
          css={{
            textAlign: 'center',
            padding: '4rem 0',
          }}>
          <div
            css={{
              marginBottom: '4rem',
            }}>
            <img
              src={getStatic('images/contact/icon-car.svg')}
              css={{
                width: 'auto',
                height: '6.4rem',
              }}
            />
          </div>
          <div css={{ fontSize: '3rem', color: '#000' }}>ส่งข้อความสำเร็จ</div>
          <div css={{ fontSize: '2.2rem', color: '#86868B' }}>
            ขอบคุณสำหรับคำแนะนำและข้อคิดเห็น <br />
            ทีมโตโยต้าได้รับข้อความของท่านเรียบร้อย
          </div>
        </div>
      </ModalMessage>
    </>
  )
}
