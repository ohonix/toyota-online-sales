import { useState } from 'react'
import Button from '@components/_common/Button'
import CustomCheckboxAgreement from '@components/_common/CustomCheckboxAgreement'
import CustomInputWithLabel from '@components/_common/CustomInputWithLabel'
import CustomLabel from '@components/_common/CustomLabel'
import CustomSelect from '@components/_common/CustomSelect'
import CustomTextarea from '@components/_common/CustomTextarea'
import ErrorFormMessage from '@components/_common/ErrorFormMessage'
import { media } from '@lib/styles/helpers'

export default function Template({ register, errors, control }) {
  const [checkedAgreement, setCheckedAgreement] = useState()
  return (
    <div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomInputWithLabel
          required
          label="ชื่อ"
          name="name"
          register={register}
          placeholder="กรุณากรอกชื่อ"
        />
        {errors.name?.message && (
          <ErrorFormMessage>{errors.name?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomInputWithLabel
          required
          label="นามสกุล"
          name="lastname"
          register={register}
          placeholder="กรุณากรอกนามสกุล"
        />
        {errors.lastname?.message && (
          <ErrorFormMessage>{errors.lastname?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomInputWithLabel
          required
          label="อีเมล"
          name="email"
          register={register}
          placeholder="กรุณากรอกอีเมล"
        />
        {errors.email?.message && (
          <ErrorFormMessage>{errors.email?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomInputWithLabel
          required
          label="เบอร์โทรศัพท์"
          name="telephone"
          register={register}
          placeholder="กรุณากรอกเบอร์โทรศัพท์"
        />
        {errors.telephone?.message && (
          <ErrorFormMessage>{errors.telephone?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomLabel required>หัวข้อ</CustomLabel>
        <CustomSelect
          name="title"
          instanceId="NkuHhN3K5"
          placeholder="โปรดระบุ"
          control={control}
          options={[
            { value: 'aaa', label: 'Lorem, ipsum.' },
            { value: 'bbb', label: 'Saepe, ipsa.' },
            { value: 'ccc', label: 'Possimus, neque.' },
          ]}
        />
        {errors.title?.message && (
          <ErrorFormMessage>{errors.title?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          marginBottom: '1.5rem',
        }}>
        <CustomLabel required>รายละเอียด</CustomLabel>
        <CustomTextarea
          name="detail"
          register={register}
          placeholder="กรุณากรอกรายละเอียด"
        />
        {errors.detail?.message && (
          <ErrorFormMessage>{errors.detail?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          paddingTop: '1.5rem',
          paddingBottom: '1.5rem',
          marginBottom: '1.5rem',
        }}>
        <CustomCheckboxAgreement
          onChange={e => {
            console.log(e, 'e aaa')
            setCheckedAgreement(e.target.checked)
          }}>
          <div
            css={{
              [media('sm')]: {
                br: {
                  display: 'none',
                },
              },
            }}>
            ข้าพเจ้าได้อ่านและยอมรับ <a href="#">ข้อตกลงและเงื่อนไข</a> <br />
            และรับทราบถึง <a href="">นโยบายความเป็นส่วนตัว</a>{' '}
            ซึ่งได้อธิบายว่าทาง บริษัท โตโยต้า มอเตอร์ ประเทศไทย จํากัด
            เก็บรวบรวม ใช้ เปิดเผย ส่งต่อ และ/หรือ
            โอนไปต่างประเทศซึ่งข้อมูลส่วนบุคคลของข้าพเจ้าอย่างไร
          </div>
        </CustomCheckboxAgreement>
        {errors.agreement?.message && (
          <ErrorFormMessage>{errors.agreement?.message}</ErrorFormMessage>
        )}
      </div>
      <div
        css={{
          textAlign: 'center',
          [media('lg')]: {
            textAlign: 'left',
          },
        }}>
        <Button
          disabled={!checkedAgreement}
          css={{
            minWidth: '16.3rem',
          }}>
          ส่งข้อมูล
        </Button>
      </div>
    </div>
  )
}
