export default function ContactAddress() {
  return (
    <address
      css={{
        paddingTop: '.4rem',
        fontStyle: 'normal',
        color: '#333333',
        margin: '0 0 3rem',
      }}>
      <h1
        css={{
          fontSize: '2.8rem',
          color: '#333333',
          fontFamily: 'db_helvethaica_x65_med',
          fontWeight: 'normal',
          margin: 0,
        }}>
        บริษัท โตโยต้า มอเตอร์ ประเทศไทย จำกัด
      </h1>
      <p
        css={{
          margin: 0,
          fontSize: '2.2rem',
        }}>
        186/1 หมู่ 1 ถ.ทางรถไฟเก่า ต.สำโรงใต้ อ.พระประแดง จ.สมุทรปราการ 10130
      </p>
    </address>
  )
}
