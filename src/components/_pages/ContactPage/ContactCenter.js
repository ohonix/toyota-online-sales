import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'

const items = [
  {
    key: 'phone',
    image: {
      src: 'images/contact/icon-phone.svg',
      height: 19,
    },
    text:
      '(66) 0-2386-2000 ตลอด 24 ชั่วโมง <br/> (หรือ 1800-238444 โทรฟรีสำหรับต่างจังหวัด เฉพาะโทรศัพท์พื้นฐาน ตลอด 24 ชั่วโมง)',
  },
  {
    key: 'email',
    image: {
      src: 'images/contact/icon-email.svg',
      height: 13,
    },
    text: 'contactcenter@toyota.co.th',
  },
  {
    key: 'line',
    image: {
      src: 'images/contact/icon-line.svg',
      height: 20,
    },
    text: '@toyotacontact',
  },
]
export default function ContactCenter() {
  return (
    <div
      css={{
        padding: '2rem 2rem .5rem',
        backgroundColor: '#F5F5F7',
        borderRadius: '.8rem',
        marginBottom: '3rem',
      }}>
      <div
        css={{
          color: '#000000',
          fontFamily: 'db_helvethaica_x65_med',
          lineHeight: 1,
          marginBottom: '1.5rem',
        }}>
        <div
          css={{
            fontSize: '2.4rem',
          }}>
          ศูนย์บริการข้อมูลลูกค้า
        </div>
        <div
          css={{
            fontSize: '1.8rem',
          }}>
          (Customer Contact Center)
        </div>
      </div>
      <ul
        css={{
          color: '#000000',
          fontSize: '2rem',
          li: {
            paddingLeft: '3.5rem',
            position: 'relative',
            marginBottom: '1.5rem',
          },
        }}>
        {items.map(({ text, image, key }) => {
          return (
            <li key={key}>
              <img
                css={{
                  position: 'absolute',
                  left: 0,
                  top: '.5rem',
                  width: 'auto',
                  height: image.height * 0.1 + 'rem',
                }}
                src={getStatic(image.src)}
              />
              <div dangerouslySetInnerHTML={{ __html: text }} />
              {key === 'line' && (
                <Image
                  src={getStatic('images/contact/icon-qrcode.png')}
                  srcSet={getStatic('images/contact/icon-qrcode@2x.png')}
                  css={{
                    marginTop: '1rem',
                    width: '9.5rem',
                    height: 'auto',
                  }}
                />
              )}
            </li>
          )
        })}
      </ul>
    </div>
  )
}
