import Breadcrumb from '@components/_common/Breadcrumb'
import { Box, Container, Flex } from '@grid'
import { Page } from '@lib/page'
import * as metaConfig from './meta'

import ContactAddress from './ContactAddress'
import ContactCenter from './ContactCenter'
import ContactForm from './ContactForm'
import { media } from '@lib/styles/helpers'
const breadcrumbItems = [
  {
    label: 'หน้าหลัก',
    route: 'home',
  },
  {
    label: 'ติดต่อเรา',
  },
]

export default function ContactPage() {
  return (
    <Page metaConfig={metaConfig}>
      <Container>
        <Breadcrumb items={breadcrumbItems} />
        <div
          css={{
            [media('lg')]: {
              display: 'flex',
              marginBottom: '6rem',
            },
          }}>
          <div
            css={{
              [media('lg')]: {
                maxWidth: '46.1rem',
                marginRight: '3rem',
              },
            }}>
            <ContactAddress />
            <ContactCenter />
          </div>
          <div
            css={{
              [media('lg')]: {
                marginLeft: 'auto',
                maxWidth: '55.8rem',
              },
            }}>
            <ContactForm />
          </div>
        </div>
      </Container>
    </Page>
  )
}
