import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

const soicalItems = [
  {
    key: 'facebook',
    image: 'images/social/icon-fb.svg',
  },
  {
    key: 'twitter',
    image: 'images/social/icon-twt.svg',
  },
  {
    key: 'line',
    image: 'images/social/icon-line.svg',
  },
]

export default function Title() {
  return (
    <>
      <h1
        css={{
          color: '#000000',
          fontSize: '3rem',
          fontWeight: 'normal',
          fontFamily: 'db_helvethaica_x65_med',
          marginBottom: '2rem',
          [media('lg')]: {
            paddingRight: '2rem',
          },
          [media('xl')]: {
            fontSize: '4.8rem',
          },
        }}>
        แพ็กเกจประกันภัย convini
      </h1>
      <div
        css={{
          display: 'flex',
          alignItems: 'center',
          marginBottom: '3rem',
        }}>
        <div
          css={{
            fontSize: '2rem',
            color: '#86868B',
            paddingRight: '1rem',
          }}>
          16 พฤศจิกายน 2563
        </div>
        <div
          css={{
            display: 'flex',
            borderLeft: '1px solid #9B9B9B',
            paddingLeft: '.5rem',
          }}>
          {soicalItems.map(({ key, image }) => (
            <div
              key={key}
              css={{
                paddingLeft: '.5rem',
                paddingRight: '.5rem',
              }}>
              <a
                href=""
                css={{
                  cursor: 'pointer',
                  display: 'block',
                  transition: 'all 0.3s',
                  '&:hover': {
                    transform: 'scale(1.2)',
                  },
                }}>
                <img
                  css={{
                    display: 'block',
                    width: '3rem',
                    height: 'auto',
                  }}
                  src={getStatic(image)}
                />
              </a>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
