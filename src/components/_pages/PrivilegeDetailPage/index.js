import Breadcrumb from '@components/_common/Breadcrumb'
import { Container } from '@grid'
import { Page } from '@lib/page'
import * as metaConfig from './meta'

import contentPrivilege from './contentPrivilege'

import { media, Responsive } from '@lib/styles/helpers'
import Title from './Title'
import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
import ButtonPrev from '@components/_common/ButtonPrev'

const breadcrumbItems = [
  {
    label: 'หน้าหลัก',
    route: 'home',
  },
  {
    label: 'สิทธิประโยชน์อีกมากมาย',
    route: 'privilegeList',
  },
  {
    label: 'แพ็กเกจประกันภัย convini',
  },
]

export default function PrivilegeDetailPage() {
  return (
    <Page metaConfig={metaConfig}>
      <Container>
        <Breadcrumb items={breadcrumbItems} />
        <Title />
        <div
          css={{
            marginBottom: '2rem',
            [media('lg')]: {
              borderTop: '1px solid #D6D6D6',
              paddingTop: '3rem',
            },
            img: {
              width: '100%',
              height: 'auto',
              display: 'block',
            },
          }}>
          <Responsive
            mobile={
              <Image
                src={getStatic('images/privilege/dummy-content-mobile.png')}
                srcSet={getStatic(
                  'images/privilege/dummy-content-mobile@2x.png',
                )}
              />
            }
            desktop={
              <Image
                src={getStatic('images/privilege/dummy-content-desktop.png')}
                srcSet={getStatic(
                  'images/privilege/dummy-content-desktop@2x.png',
                )}
              />
            }
          />
        </div>

        <div
          css={{
            color: '#000000',
            marginBottom: '2rem',
            p: {
              marginBottom: '2rem',
            },
            a: {
              color: '#86868B',
              textDecoration: 'underline',
              '&:hover': {
                color: '#ED1B2F',
              },
            },
          }}
          dangerouslySetInnerHTML={{ __html: contentPrivilege }}
        />
        <div
          css={{
            borderTop: '1px solid #9B9B9B',
            paddingTop: '2.4rem',
            paddingBottom: '5rem',
            textAlign: 'center',
            [media('lg')]: {
              textAlign: 'left',
            },
          }}>
          <ButtonPrev route="privilegeList">กลับหน้าหลัก</ButtonPrev>
        </div>
      </Container>
    </Page>
  )
}
