import { useState, Fragment, useEffect } from 'react'
import { getStatic } from '@lib/static'
import { useTheme } from '@emotion/react'
import { animated, useSpring } from 'react-spring'
import useMeasure from 'react-use-measure'
import { darken } from 'polished'
import { media } from '@lib/styles/helpers'
export default function CollapseContainer({
  children,
  onToggleCollapse = () => null,
}) {
  const { variables } = useTheme()
  const [collapsed, setCollapsed] = useState()
  const [containerMeasureRef, containerMeasureBounds] = useMeasure()
  const [titleMeasureRef, titleMeasureBounds] = useMeasure()

  const styles = useSpring({
    to: {
      height: collapsed
        ? titleMeasureBounds.height
        : containerMeasureBounds.height,
    },
  })

  function toggleCollapse() {
    setCollapsed(collapsed => !collapsed)
  }

  useEffect(() => {
    onToggleCollapse(collapsed)
  }, [collapsed])

  return (
    <Fragment>
      <animated.div
        style={{
          overflow: 'hidden',
          ...styles,
        }}>
        <div ref={containerMeasureRef}>
          <div>{children({ titleMeasureRef })}</div>
        </div>
      </animated.div>
      <div
        css={{
          textAlign: 'center',
        }}>
        <div
          onClick={toggleCollapse}
          css={{
            cursor: 'pointer',
            padding: '1rem',
            display: 'inline-flex',
            alignItems: 'center',
            justifyContent: 'center',
            color: variables.colors.highlight.lvl1,
            fontSize: '2.4rem',
            '&:active': {
              color: darken(0.2, variables.colors.highlight.lvl1),
            },
            [media('xl')]: {
              '&:hover': {
                color: darken(0.2, variables.colors.highlight.lvl1),
              },
            },
          }}>
          <div>
            <img
              css={{
                display: 'block',
                marginRight: '1rem',
                transition: 'all 0.3s',
                ...(collapsed && { transform: 'rotate(180deg)' }),
              }}
              src={getStatic('images/home-highlight/icon-chevron-top1.svg')}
            />
          </div>
          <div>{!collapsed ? 'ซ่อน' : 'เลือกรุ่นรถ'}</div>
        </div>
      </div>
    </Fragment>
  )
}
