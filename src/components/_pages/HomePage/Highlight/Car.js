import { getStatic } from '@lib/static'
import CarInfo from './CarInfo'
import Image from '@components/_common/Image'
import FlyIn from '@components/_common/FlyIn'
import Button from '@components/_common/Button'
import ButtonMore from '@components/_common/ButtonMore'
import { media } from '@lib/styles/helpers'

export default function Car() {
  return (
    <div
      css={{
        textAlign: 'center',
        [media('lg')]: {
          display: 'flex',
          flexDirection: 'column',
          paddingTop: '7rem',
        },
      }}>
      <div
        css={{
          [media('lg')]: {
            order: 1,
            maxWidth: '70rem',
            width: '100%',
            margin: '-3rem auto 0',
          },
          img: {
            width: '100%',
            height: 'auto',
          },
        }}>
        <FlyIn directionTo="left" delay={500}>
          <Image
            src={getStatic('images/home-highlight/car-vios.png')}
            srcSet={getStatic('images/home-highlight/car-vios@2x.png')}
          />
        </FlyIn>
      </div>
      <div
        css={{
          [media('lg')]: {
            order: 0,
          },
        }}>
        <FlyIn delay={600}>
          <div
            css={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: '1.5rem',
            }}>
            <Image
              src={getStatic('images/home-highlight/car-vios-logo.png')}
              srcSet={getStatic('images/home-highlight/car-vios-logo@2x.png')}
            />
            <div
              css={{
                marginLeft: '1.5rem',
              }}>
              <CarInfo />
            </div>
          </div>
        </FlyIn>
        <div>
          <FlyIn delay={800}>
            <div
              css={{
                fontSize: '2.4rem',
                color: '#000',
                marginBottom: '.5rem',
                [media('lg')]: {
                  fontSize: '2.2rem',
                  color: '#86868B',
                  br: {
                    display: 'none',
                  },
                },
              }}>
              ดีไซน์พรีเมียมเหนือระดับสมรรถนะ <br />
              ที่เหนือกว่า ให้คุณทำทุกสิ่งได้ตามใจ
            </div>
          </FlyIn>
          <FlyIn delay={1000}>
            <div
              css={{
                fontSize: '2.8rem',
                color: '#86868B',
                marginBottom: '2rem',
                [media('lg')]: {
                  fontSize: '3.2rem',
                  color: '#000',
                },
              }}>
              เริ่มต้น 609,000 บาท
            </div>
          </FlyIn>
        </div>
      </div>
      <div
        css={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          order: 2,
        }}>
        <div>
          <Button>จองเลย</Button>
        </div>
        <div
          css={{
            marginLeft: '1rem',
            [media('lg')]: {
              marginLeft: '5rem',
            },
          }}>
          <ButtonMore route="customize">แต่งรถ</ButtonMore>
        </div>
      </div>
    </div>
  )
}
