import FlyIn from '@components/_common/FlyIn'
import { useTheme } from '@emotion/react'
import { media } from '@lib/styles/helpers'

export default function Title() {
  const { variables } = useTheme()
  return (
    <div
      css={{
        textAlign: 'center',
        [media('lg')]: {
          textAlign: 'left',
          maxWidth: '85rem',
          width: '100%',
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      }}>
      <FlyIn delay={100}>
        <div
          css={{
            fontSize: '2.2rem',
            lineHeight: 1,
            color: '#000000',
            [media('lg')]: {
              fontSize: '3.4rem',
            },
          }}>
          WELCOME TO
        </div>
      </FlyIn>

      <div
        css={{
          [media('lg')]: {
            display: 'flex',
            alignItems: 'center',
          },
        }}>
        <FlyIn delay={200}>
          <div
            css={{
              fontSize: '7.2rem',
              lineHeight: 0.8,
              color: variables.colors.highlight.lvl1,
              fontFamily: 'db_helvethaica_x75_bd',
              [media('lg')]: {
                fontSize: '17.6rem',
              },
            }}>
            TOYOTA
          </div>
        </FlyIn>
        <div
          css={{
            marginLeft: 'auto',
          }}>
          <FlyIn delay={300}>
            <div
              css={{
                fontSize: '5.4rem',
                lineHeight: 1,
                marginTop: '-1rem',
                color: '#000',
                fontFamily: 'db_helvethaica_x65_med',
                [media('lg')]: {
                  fontSize: '8rem',
                },
              }}>
              online sales
            </div>
          </FlyIn>
          <FlyIn delay={400}>
            <div
              css={{
                fontSize: '2.1rem',
                lineHeight: 1,
                color: '#000',
                fontFamily: 'db_helvethaica_x45_li',
                [media('lg')]: {
                  fontSize: '3.4rem',
                },
              }}>
              รับข้อเสนอสุดพิเศษ เมื่อออกรถที่นี่!
            </div>
          </FlyIn>
        </div>
      </div>
    </div>
  )
}
