import { useState } from 'react'
import { getStatic } from '@lib/static'
import { Container } from '@grid'
import Title from './Title'
import Car from './Car'
import CollapseContainer from './CollapseContainer'
import { media } from '@lib/styles/helpers'

export default function Highlight() {
  const [collapsed, setCollapsed] = useState()

  function onToggleCollapse(collapsed) {
    setCollapsed(collapsed)
  }
  return (
    <div
      css={{
        backgroundImage: `url(
        ${getStatic('images/home-highlight/bg-home-highlight.png')}
        )`,
        backgroundSize: '99.3rem 43.9rem',
        backgroundPosition: 'center 3.5rem',
        backgroundRepeat: 'no-repeat',
        transition: 'all 0.3s',
        ...(collapsed && {
          backgroundPosition: 'center 33%',
        }),
        [media('lg')]: {
          backgroundImage: `url(
            ${getStatic('images/home-highlight/bg-home-highlight-lg.png')}
            )`,
          backgroundSize: '218.4rem 96.5rem',
          backgroundPosition: 'center 3.5rem',
          ...(collapsed && {
            backgroundPosition: 'center 46%',
          }),
        },
      }}>
      <CollapseContainer onToggleCollapse={onToggleCollapse}>
        {({ titleMeasureRef }) => (
          <Container>
            <div css={{ paddingBottom: '2rem' }}>
              <div
                ref={titleMeasureRef}
                css={{
                  paddingTop: '2rem',
                  paddingBottom: '1rem',
                  [media('lg')]: {
                    paddingTop: '3rem',
                  },
                }}>
                <Title />
              </div>
              <Car />
            </div>
          </Container>
        )}
      </CollapseContainer>
    </div>
  )
}
