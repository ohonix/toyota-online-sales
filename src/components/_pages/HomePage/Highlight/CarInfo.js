import { Fragment, useState, memo } from 'react'
import { useSpring, animated, config } from 'react-spring'
import { useDetectClickOutside } from 'react-detect-click-outside'

import { getStatic } from '@lib/static'

import CarInfoBubble from './CarInfoBubble'

function CarInfo() {
  const [toggle, setToggle] = useState()
  const stylesIconToggle = useSpring({
    to: { opacity: 1, scale: 1 },
    from: { opacity: 0, scale: 0 },
    reset: true,
    config: config.wobbly,
  })

  const ref = useDetectClickOutside({ onTriggered: () => setToggle(false) })

  function handleToggle() {
    setToggle(toggle => !toggle)
  }

  return (
    <div
      ref={ref}
      css={{
        position: 'relative',
        cursor: 'pointer',
      }}>
      {!toggle ? (
        <animated.div style={stylesIconToggle}>
          <img
            onClick={handleToggle}
            width="20"
            height="20"
            css={{
              display: 'block',
            }}
            src={getStatic('images/home-highlight/icon-info.svg')}
          />
        </animated.div>
      ) : (
        <Fragment>
          <animated.div style={stylesIconToggle}>
            <img
              onClick={handleToggle}
              width="20"
              height="20"
              css={{
                display: 'block',
              }}
              src={getStatic('images/home-highlight/icon-info-active.svg')}
            />
            <CarInfoBubble />
          </animated.div>
        </Fragment>
      )}
    </div>
  )
}

function areEqual(prevProps, nextProps) {
  return true
}
export default memo(CarInfo, areEqual)
