import { getStatic } from '@lib/static'
export default function CarInfoBubble() {
  return (
    <div
      css={{
        position: 'absolute',
        left: '50%',
        top: '100%',
        transform: 'translateX(-50%)',
        marginTop: '1.2rem',
        width: '20rem',
        padding: '1rem',
        borderRadius: '.4rem',
        backgroundColor: '#fff',
        boxShadow: '0 .3rem 1rem rgba(0,0,0, 10%)',
        fontSize: '1.58rem',
        textAlign: 'left',
        lineHeight: '1',
      }}>
      <div
        css={{
          color: '#000',
          fontFamily: 'db_helvethaica_x65_med',
        }}>
        รายละเอียดรถ
      </div>
      <div>
        <a
          href="https://www.toyota.co.th/model/vios"
          target="_blank"
          rel="noreferrer"
          css={{
            color: '#D70000',
            textDecoration: 'underline',
          }}>
          https://www.toyota.co.th/model/vios
        </a>
      </div>
      <img
        width="13"
        height="7"
        css={{
          display: 'block',
          position: 'absolute',
          left: '50%',
          bottom: '100%',
          transform: 'translateX(-50%)',
        }}
        src={getStatic('images/home-highlight/icon-bubble-rect.svg')}
      />
    </div>
  )
}
