import { getStatic } from '@lib/static'
import { media, Responsive } from '@lib/styles/helpers'

import { Container } from '@grid'
import Title from './Title'
import Features from './Features'
import Buttons from './Buttons'

export default function Intro() {
  return (
    <div
      css={{
        minHeight: '65.1rem',
        padding: '31.3rem 0 0',
        backgroundSize: 'auto 65.1rem',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundColor: '#1A1C1E',
        color: '#fff',
        backgroundImage: `url( ${getStatic(
          'images/home-intro/bg-home-intro-mobile@2x.png',
        )} )`,
        [media('xl')]: {
          backgroundImage: `url( ${getStatic(
            'images/home-intro/bg-home-intro-desktop@2x.png',
          )})`,
          backgroundSize: 'auto 100%',
          backgroundPosition: 'center',
          minHeight: '0',
          padding: 0,
        },
      }}>
      <Container>
        <div
          css={{
            [media('xl')]: {
              height: '75rem',
              position: 'relative',
            },
          }}>
          <div
            css={{
              [media('xl')]: {
                position: 'absolute',
                right: 0,
                top: '50%',
                transform: 'translateY(-50%)',
              },
            }}>
            <Title />
            <Responsive breakpoint="xl" desktop={<Buttons />} />
          </div>
          <div
            css={{
              [media('xl')]: {
                position: 'absolute',
                left: 0,
                right: 0,
                bottom: '7.5rem',
              },
            }}>
            <Features />
          </div>
          <Responsive breakpoint="xl" mobile={<Buttons />} />
        </div>
      </Container>
    </div>
  )
}
