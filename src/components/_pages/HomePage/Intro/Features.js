import { Flex, Box } from '@grid'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

const items = [
  {
    label: 'หน้าจอเชื่อมต่อแอพพลิเคชั่น',
    image: 'images/home-intro/icon-feature-bluetooth.svg',
  },
  {
    label: 'Push Start Smart Entry',
    image: 'images/home-intro/icon-feature-push-start.svg',
  },
  {
    label: 'กล้องบันทึกภาพหน้ารถ',
    image: 'images/home-intro/icon-feature-front-camera.svg',
  },
]

export default function Features() {
  return (
    <div
      css={{
        marginBottom: '4rem',
        [media('xl')]: {
          marginBottom: '0rem',
        },
      }}>
      <Flex
        css={{
          '> div:not(:last-of-type)': {
            position: 'relative',
            '&:after': {
              content: '""',
              display: 'block',
              height: '100%',
              width: 1,
              backgroundColor: `rgba(255,255,255,0.5)`,
              position: 'absolute',
              right: 0,
              top: 0,
            },
          },
        }}>
        {items.map((item, i) => (
          <Box key={i} width={[1 / 3]}>
            <div
              css={{
                position: 'relative',
                height: '100%',
                padding: '1.1rem 1.4rem',
              }}>
              <div
                css={{
                  marginBottom: '1rem',
                  [media('xl')]: {
                    marginBottom: '1.5rem',
                  },
                }}>
                <img
                  css={{
                    display: 'block',
                    width: 'auto',
                    height: '2.2rem',
                    margin: 'auto',
                    [media('xl')]: {
                      height: '2.7rem',
                    },
                  }}
                  src={getStatic(item.image)}
                />
              </div>
              <div
                css={{
                  textAlign: 'center',
                  fontSize: '1.8rem',
                  lineHeight: 1,
                  [media('xl')]: {
                    fontSize: '2.4rem',
                  },
                }}>
                {item.label}
              </div>
            </div>
          </Box>
        ))}
      </Flex>
    </div>
  )
}
