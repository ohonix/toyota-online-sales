import Button from '@components/_common/Button'
import ButtonMore from '@components/_common/ButtonMore'
export default function Buttons() {
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <div>
        <Button>จองเลย</Button>
      </div>
      <div
        css={{
          marginLeft: '1rem',
        }}>
        <ButtonMore>รายละเอียด</ButtonMore>
      </div>
    </div>
  )
}
