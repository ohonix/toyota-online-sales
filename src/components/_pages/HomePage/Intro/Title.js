import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'
import Image from '@components/_common/Image'
export default function Title() {
  return (
    <div
      css={{
        textAlign: 'center',
        marginBottom: '1.6rem',
      }}>
      <div
        css={{
          fontSize: 0,
          marginBottom: '1.2rem',
        }}>
        <Image
          width="103"
          height="20"
          src={getStatic('images/home-intro/icon-vios.png')}
          srcSet={getStatic('images/home-intro/icon-vios@2x.png')}
          css={{
            width: 'auto',
            height: '2rem',
            margin: 'auto',
            [media('xl')]: {
              height: '3rem',
            },
          }}
        />
      </div>
      <div
        css={{
          fontSize: '4.6rem',
          [media('xl')]: {
            fontSize: '5.6rem',
          },
        }}>
        ครบเต็มคัน คุ้มเกินใคร
      </div>
    </div>
  )
}
