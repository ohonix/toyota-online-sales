import ButtonMore from '@components/_common/ButtonMore'

export default function Title() {
  return (
    <div
      css={{
        textAlign: 'center',
        lineHeight: '1',
      }}>
      <div
        css={{
          fontSize: '4.6rem',
          color: '#000000',
          fontFamily: 'db_helvethaica_x65_med',
        }}>
        ชุดแต่งราคาสุดพิเศษ
      </div>
      <div
        css={{
          color: '#86868B',
          fontSize: '2.4rem',
        }}>
        แต่งรถสไตล์คุณด้วยพาร์ทเนอร์ของเรา
      </div>
      <div>
        <ButtonMore>ดูชุดแต่งทั้งหมด</ButtonMore>
      </div>
    </div>
  )
}
