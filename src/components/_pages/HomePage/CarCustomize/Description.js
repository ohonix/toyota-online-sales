import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

export default function Description({ description, brandImage, brandImage2x }) {
  return (
    <div
      css={{
        textAlign: 'center',
        maxWidth: '30rem',
        width: '100%',
        margin: 'auto',
        [media('lg')]: {
          textAlign: 'left',
          maxWidth: 'none',
        },
      }}>
      <div
        css={{
          fontSize: '3rem',
          color: '#222222',
          fontFamily: 'db_helvethaica_x65_med',
          marginBottom: '1rem',
          [media('lg')]: {
            fontSize: '4rem',
            marginBottom: '1.6rem',
          },
        }}>
        {description}
      </div>

      <div
        css={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: '1.5rem',
          [media('lg')]: {
            justifyContent: 'flex-start',
            marginBottom: '3rem',
          },
          img: {
            width: 'auto',
            height: '3.6rem',
            display: 'block',
          },
        }}>
        <div
          css={{
            fontSize: '2rem',
            color: '#222222',
            fontFamily: 'db_helvethaica_x65_med',
            marginRight: '.7rem',
          }}>
          By
        </div>
        <div>
          <Image
            width="114"
            height="36"
            src={getStatic(brandImage)}
            srcSet={getStatic(brandImage2x)}
          />
        </div>
      </div>
      <div
        css={{
          color: '#86868B',
          fontSize: '2.2rem',
          span: {
            textDecoration: 'line-through',
          },
        }}>
        จาก <span>50,000</span>
      </div>
      <div
        css={{
          fontSize: '2.2rem',
          color: '#000',
          marginBottom: '2rem',
        }}>
        เหลือ 32,000 บาท
      </div>
    </div>
  )
}
