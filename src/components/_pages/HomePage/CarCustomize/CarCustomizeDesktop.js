import { Container } from '@grid'

import Car from './Car'
import Buttons from './Buttons'
import Description from './Description'

const items = [
  {
    image: 'images/home-customize/desktop/car-dummy-1.png',
    image2x: 'images/home-customize/desktop/car-dummy-1@2x.png',
    description: 'คำโปรยที่เชิญชวนให้อยาก แต่งรถ คำโปรยที่เชิญชวน',
    brandImage: 'images/home-customize/desktop/brand1.png',
    brandImage2x: 'images/home-customize/desktop/brand1@2x.png',
    price: '50,000',
    priceDiscount: '32,000',
    linkBuy: '',
    linkDetail: '',
  },
  {
    image: 'images/home-customize/desktop/car-dummy-2.png',
    image2x: 'images/home-customize/desktop/car-dummy-2@2x.png',
    description: 'คำโปรยที่เชิญชวนให้อยาก แต่งรถ คำโปรยที่เชิญชวน',
    brandImage: 'images/home-customize/desktop/brand2.png',
    brandImage2x: 'images/home-customize/desktop/brand2@2x.png',
    price: '50,000',
    priceDiscount: '32,000',
    linkBuy: '',
    linkDetail: '',
  },
  {
    image: 'images/home-customize/desktop/car-dummy-3.png',
    image2x: 'images/home-customize/desktop/car-dummy-3@2x.png',
    description: 'คำโปรยที่เชิญชวนให้อยาก แต่งรถ คำโปรยที่เชิญชวน',
    brandImage: 'images/home-customize/desktop/brand1.png',
    brandImage2x: 'images/home-customize/desktop/brand1@2x.png',
    price: '50,000',
    priceDiscount: '32,000',
    linkBuy: '',
    linkDetail: '',
  },
]

export default function CarCustomizeDesktop() {
  return items.map((item, i) => {
    const isOdd = i % 2
    return (
      <div
        key={i}
        css={{
          overflow: 'hidden',
          marginBottom: '5rem',
        }}>
        <Container>
          <div
            css={{
              display: 'flex',
            }}>
            <div
              css={{
                width: '65%',
                display: 'flex',
                justifyContent: isOdd ? 'flex-start' : 'flex-end',
                order: isOdd ? 1 : 0,
              }}>
              <Car image={item.image} image2x={item.image2x} />
            </div>
            <div
              css={{
                width: '35%',
                padding: '3rem 2rem 0',
              }}>
              <Description
                description={item.description}
                brandImage={item.brandImage}
                brandImage2x={item.brandImage2x}
              />
              <Buttons linkBuy={item.linkBuy} linkDetail={item.linkDetail} />
            </div>
          </div>
        </Container>
      </div>
    )
  })
}
