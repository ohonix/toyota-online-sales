import { Container } from '@grid'
import { Responsive } from '@lib/styles/helpers'

import Title from './Title'
import CarCustomizeMobile from './CarCustomizeMobile'
import CarCustomizeDesktop from './CarCustomizeDesktop'

export default function CarCustomize() {
  return (
    <div
      css={{
        backgroundColor: '#F2F2F2',
        paddingTop: '3.8rem',
        paddingBottom: '9.6rem',
        '.swiper-container': {
          paddingBottom: '4rem',
        },
        '.swiper-container-horizontal>.swiper-pagination-bullets, .swiper-pagination-custom, .swiper-pagination-fraction': {
          bottom: 0,
        },
      }}>
      <Container>
        <Title />
      </Container>
      <Responsive
        breakpoint="lg"
        desktop={<CarCustomizeDesktop />}
        mobile={<CarCustomizeMobile />}
      />
    </div>
  )
}
