import Button from '@components/_common/Button'
import ButtonMore from '@components/_common/ButtonMore'
import { media } from '@lib/styles/helpers'

export default function Buttons({ linkBuy, linkDetail }) {
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        [media('lg')]: {
          justifyContent: 'flex-start',
        },
      }}>
      <div>
        <Button>ซื้อรถ+ชุดแต่งนี้</Button>
      </div>
      <div
        css={{
          marginLeft: '1rem',
        }}>
        <ButtonMore>ดูเพิ่มเติม</ButtonMore>
      </div>
    </div>
  )
}
