import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper.min.css'
import 'swiper/components/pagination/pagination.min.css'
import { Container } from '@grid'

import Car from './Car'
import Buttons from './Buttons'
import Description from './Description'

import SwiperCore, { Pagination } from 'swiper/core'
SwiperCore.use([Pagination])

const items = [1, 2, 3]

export default function CarCustomizeMobile() {
  return (
    <Swiper
      pagination={{
        dynamicBullets: true,
      }}
      spaceBetween={50}>
      {items.map((item, i) => (
        <SwiperSlide key={i}>
          <div css={{ overflow: 'hidden' }}>
            <Car
              image="images/home-customize/car.png"
              image2x="images/home-customize/car@2x.png"
            />
            <Container>
              <Description
                description="คำโปรยที่เชิญชวนให้อยากแต่งรถ คำโปรยที่เชิญชวนให้อยากแต่งรถ"
                brandImage="images/home-customize/brand-1.png"
                brandImage2x="images/home-customize/brand-1@2x.png"
              />
            </Container>
            <Container>
              <Buttons />
            </Container>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  )
}
