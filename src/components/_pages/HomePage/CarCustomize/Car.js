import Image from '@components/_common/Image'
import FlyIn from '@components/_common/FlyIn'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

export default function Car({ image, image2x }) {
  return (
    <div>
      <div
        css={{
          height: '32rem',
          width: '100%',
          position: 'relative',
          zIndex: '3',
          marginBottom: '2rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          [media('lg')]: {
            justifyContent: 'flex-end',
            width: '113.9rem',
            height: '46.6rem',
            marginBottom: 0,
          },
          img: {
            height: '100%',
            width: 'auto',
            maxWidth: 'none',
          },
        }}>
        <FlyIn directionTo="right" delay={400}>
          <Image src={getStatic(image)} srcSet={getStatic(image2x)} />
        </FlyIn>
      </div>
    </div>
  )
}
