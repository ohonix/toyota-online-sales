import { Container } from '@grid'
import { media } from '@lib/styles/helpers'

export default function HomeContact() {
  return (
    <div
      css={{
        backgroundColor: '#FBFBFD',
        padding: '4rem 0',
        color: '#000',
        textAlign: 'center',
        [media('xl')]: {
          padding: '6rem 0',
        },
      }}>
      <Container>
        <div
          css={{
            fontSize: '3.2rem',
            letterSpacing: '.2rem',
            [media('xl')]: {
              fontSize: '4.8rem',
            },
          }}>
          ศูนย์บริการข้อมูลลูกค้า
        </div>
        <div
          css={{
            fontSize: '2.2rem',
            [media('xl')]: {
              fontSize: '4rem',
            },
          }}>
          <span
            css={{
              fontSize: '3.6rem',
              fontFamily: 'db_helvethaica_x65_med',
              marginRight: '1rem',
              textDecoration: 'underline',
              [media('xl')]: {
                fontSize: '6.4rem',
              },
            }}>
            (66) 0-2386-2000
          </span>
          ตลอด 24 ชั่วโมง
        </div>
        <div
          css={{
            fontSize: '2.2rem',
            color: '#86868B',
            [media('xl')]: {
              fontSize: '3.2rem',
            },
          }}>
          contactcenter@toyota.co.th
        </div>
      </Container>
    </div>
  )
}
