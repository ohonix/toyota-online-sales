import { getStatic } from '@lib/static'
import { Container } from '@grid'
import Title from './Title'
import Buttons from './Buttons'
import { media } from '@lib/styles/helpers'

export default function SpecialOffer() {
  return (
    <div
      css={{
        color: '#fff',
        backgroundColor: '#000',
        backgroundSize: 'auto 58rem',
        backgroundPosition: 'center bottom',
        backgroundRepeat: 'no-repeat',
        backgroundImage: `url(${getStatic(
          'images/home-special-offer/bg-home-special-offer@2x.png',
        )})`,
        [media('xl')]: {
          backgroundImage: `url(${getStatic(
            'images/home-special-offer/bg-home-special-offer-desktop@2x.png',
          )})`,
          backgroundSize: 'auto 51.6rem',
        },
      }}>
      <Container>
        <div
          css={{
            paddingTop: '4.6rem',
            minHeight: '68.4rem',
            [media('xl')]: {
              paddingTop: 0,
              minHeight: 0,
              height: '51.6rem',
              display: 'flex',
              alignItems: 'center',
            },
          }}>
          <div>
            <Title />
            <Buttons />
          </div>
        </div>
      </Container>
    </div>
  )
}
