import { useState } from 'react'
import Button from '@components/_common/Button'
import ButtonMore from '@components/_common/ButtonMore'
import ModalArticle from '@components/_common/ModalArticle'
import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'

function ModalFooter() {
  return (
    <div
      css={{
        textAlign: 'center',
      }}>
      <Button>จองเลย</Button>
    </div>
  )
}
export default function ButtonModal() {
  const [modalIsOpen, setIsOpen] = useState(false)
  function openModal() {
    setIsOpen(true)
  }
  function closeModal() {
    setIsOpen(false)
  }

  return (
    <>
      <ButtonMore onClick={openModal}>รายละเอียด</ButtonMore>
      <ModalArticle
        isOpen={modalIsOpen}
        closeModal={closeModal}
        footer={<ModalFooter />}>
        <div
          css={{
            color: '#000000',
            fontSize: '3rem',
            marginBottom: '2rem',
            fontFamily: 'db_helvethaica_x65_med',
          }}>
          รายละเอียดข้อเสนอพิเศษ
        </div>
        <div
          css={{
            paddingBottom: '2rem',
            marginBottom: '1.5rem',
            borderBottom: '1px solid #CDCDCD',
            img: {
              width: '100%',
              height: 'auto',
            },
          }}>
          <Image
            src={getStatic('images/home-special-offer/dummy-thumbnail.png')}
            srcSet={getStatic(
              'images/home-special-offer/dummy-thumbnail@2x.png',
            )}
          />
        </div>
        <div
          css={{
            color: '#000000',
            fontSize: '2.4rem',
            marginBottom: '1rem',
            fontFamily: 'db_helvethaica_x65_med',
          }}>
          รายละเอียด
        </div>
        <div
          css={{
            color: '#000000',
            u: {
              textDecoration: 'underline',
            },
            ul: {
              listStyle: 'disc',
              paddingLeft: '2rem',
            },
          }}>
          <p>ดาวน์ 0% ผ่อนเริ่มต้น 7,200 บาทพร้อมรับของแถมอีกมากมาย</p>
          <ul>
            <li>
              ฟรี <u>ของแถม 10 รายการ</u> <span>มูลค่า 40,000 บาท</span>
            </li>
            <li>
              ฟรี <u>ประกันชั้น 1</u> <span>มูลค่า 25,000 บาท</span>
            </li>
            <li>
              ฟรี <u>Toyota Privilege</u> <span>มูลค่า 40,000 บาท</span>
            </li>
          </ul>
        </div>
      </ModalArticle>
    </>
  )
}
