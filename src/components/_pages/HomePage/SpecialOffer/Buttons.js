import Button from '@components/_common/Button'
import { media } from '@lib/styles/helpers'
import ButtonModal from './ButtonModal'

export default function Buttons() {
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        [media('xl')]: {
          justifyContent: 'flex-start',
        },
      }}>
      <div>
        <Button>จองเลย</Button>
      </div>
      <div
        css={{
          marginLeft: '1rem',
        }}>
        <ButtonModal />
      </div>
    </div>
  )
}
