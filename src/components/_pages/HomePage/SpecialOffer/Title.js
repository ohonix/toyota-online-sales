import { media } from '@lib/styles/helpers'

export default function Title() {
  return (
    <div
      css={{
        textAlign: 'center',
      }}>
      <div
        css={{
          [media('xl')]: {
            display: 'flex',
            alignItems: 'baseline',
          },
        }}>
        <div
          css={{
            fontSize: '2.4rem',
            color: '#fff',
            [media('xl')]: {
              fontSize: '4rem',
              marginRight: '2rem',
            },
          }}>
          ข้อเสนอพิเศษ
        </div>
        <div
          css={{
            fontSize: '4.6rem',
            color: '#ED1B2F',
            [media('xl')]: {
              fontSize: '8.4rem',
            },
          }}>
          จองวันนี้!
        </div>
      </div>
      <div
        css={{
          fontSize: '2.4rem',
          color: '#86868B',
          marginBottom: '3rem',
          [media('xl')]: {
            fontSize: '3.2rem',
            textAlign: 'left',
          },
          br: {
            display: 'none',
            [media('xl')]: {
              display: 'block',
            },
          },
        }}>
        ดาวน์ 0% ผ่อนเริ่มต้น 7,200 บาท <br /> พร้อมรับของแถมอีกมากมาย
      </div>
    </div>
  )
}
