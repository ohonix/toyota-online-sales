import { Page } from '@lib/page'
import * as metaConfig from './meta'
import Highlight from './Highlight'
import Intro from './Intro'
import CarCustomize from './CarCustomize'
import SpecialOffer from './SpecialOffer'
import Privilege from './Privilege'
import HomeContact from './HomeContact'

export default function HomePage() {
  return (
    <Page metaConfig={metaConfig}>
      <Highlight />
      <Intro />
      <CarCustomize />
      <SpecialOffer />
      <Privilege />
      <HomeContact />
    </Page>
  )
}
