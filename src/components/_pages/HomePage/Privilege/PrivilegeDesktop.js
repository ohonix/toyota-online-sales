import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper.min.css'
import 'swiper/components/pagination/pagination.min.css'
import SwiperCore, { Pagination } from 'swiper/core'

import Thumbnail from './Thumbnail'

SwiperCore.use([Pagination])

const items = [1, 2, 3, 4, 5, 6, 8]

export default function PrivilegeDesktop() {
  return (
    <div
      css={{
        marginBottom: '50px',
        '.swiper-pagination-bullets': {
          bottom: '-1rem',
        },
        '.swiper-pagination-bullet': {
          backgroundColor: '#fff',
        },
      }}>
      <Swiper
        slidesPerView={3}
        pagination={{
          dynamicBullets: true,
        }}
        spaceBetween={30}>
        {items.map((item, i) => (
          <SwiperSlide key={i}>
            <Thumbnail key={i} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}
