import { getStatic } from '@lib/static'
import { media, Responsive } from '@lib/styles/helpers'
import { Container } from '@grid'

import Title from './Title'
import PrivilegeMobile from './PrivilegeMobile'
import PrivilegeDesktop from './PrivilegeDesktop'
import PrivilegeSeemore from './PrivilegeSeemore'

export default function Privilege() {
  return (
    <div
      css={{
        backgroundRepeat: 'no-repeat',
        backgroundColor: '#000',
        backgroundImage: `url(${getStatic(
          'images/home-privilege/bg-home-privilege-below@2x.png',
        )})`,
        backgroundSize: '100% auto',
        backgroundPosition: 'center bottom',
        [media('lg')]: {
          backgroundImage: `url(${getStatic(
            'images/home-privilege/bg-home-privilege-below-desktop@2x.png',
          )})`,
          backgroundSize: 'auto 643px',
        },
      }}>
      <div
        css={{
          backgroundImage: `url(${getStatic(
            'images/home-privilege/bg-home-privilege@2x.png',
          )})`,
          backgroundSize: '100% auto',
          backgroundPosition: 'center top',
          backgroundRepeat: 'no-repeat',
          backgroundColor: 'transparent',
          paddingTop: '4rem',
          paddingBottom: '13rem',
          overflow: 'hidden',
          '.swiper-container': {
            overflow: 'visible',
          },
          [media('lg')]: {
            backgroundImage: `url(${getStatic(
              'images/home-privilege/bg-home-privilege-desktop@2x.png',
            )})`,
            backgroundSize: 'auto 370px',
          },
          [media('xl')]: {
            paddingTop: '5.1rem',
            paddingBottom: '7rem',
          },
        }}>
        <Container>
          <Title />
          <Responsive
            breakpoint="xl"
            mobile={<PrivilegeMobile />}
            desktop={<PrivilegeDesktop />}
          />
          <PrivilegeSeemore />
        </Container>
      </div>
    </div>
  )
}
