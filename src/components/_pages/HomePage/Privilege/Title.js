import { media } from '@lib/styles/helpers'

export default function Title() {
  return (
    <div
      css={{
        textAlign: 'center',
        marginBottom: '3rem',
        [media('xl')]: {
          marginBottom: '6rem',
        },
      }}>
      <div
        css={{
          fontSize: '4.6rem',
          color: '#ED1B2F',
          lineHeight: 1,
          [media('xl')]: {
            fontSize: '5.6rem',
          },
        }}>
        สิทธิประโยชน์อีกมากมาย
      </div>
      <div
        css={{
          fontSize: '2.4rem',
          color: '#86868B',
          [media('xl')]: {
            fontSize: '2.8rem',
          },
        }}>
        เพื่อให้เราได้ดูแลคนพิเศษเช่นคุณ
      </div>
    </div>
  )
}
