import { useTheme } from '@emotion/react'
import { Flex, Box } from '@grid'
import { getStatic } from '@lib/static'
import Thumbnail from './Thumbnail'
const items = [1, 2, 3, 4]
export default function PrivilegeMobile() {
  return (
    <div
      css={{
        marginBottom: '3rem',
      }}>
      <Flex>
        {items.map((item, i) => (
          <Box key={i} width={[1, 1 / 2]}>
            <div
              css={{
                padding: '0 1rem',
              }}>
              <Thumbnail />
            </div>
          </Box>
        ))}
      </Flex>
    </div>
  )
}
