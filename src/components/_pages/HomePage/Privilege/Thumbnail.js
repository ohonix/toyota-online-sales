import ButtonMore from '@components/_common/ButtonMore'
import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'
import CustomLink from '@link'

export default function Thumbnail({ isLightTheme }) {
  return (
    <div
      css={{
        textAlign: 'center',
        marginBottom: '2rem',
        overflow: 'hidden',
        borderRadius: '1.6rem',
        backgroundColor: 'rgba(255,255,255, 0.05)',
        border: '1px solid rgba(255,255,255, 0.1)',
        transition: 'all 0.3s',
        [media('xl')]: {
          '&:hover': {
            backgroundColor: 'rgba(255,255,255, 0.1)',
            border: '1px solid rgba(255,255,255, 0.15)',
          },
        },
        ...(isLightTheme && {
          backgroundColor: '#F2F2F2',
          borderColor: 'transparent',
          [media('xl')]: {
            '&:hover': {
              backgroundColor: '#ffff',
              borderColor: '#F2F2F2',
            },
          },
        }),
      }}>
      <CustomLink route="privilegeDetail" params={{ id: 'detail' }}>
        <a>
          <div
            css={{
              overflow: 'hidden',
              borderRadius: '1.6rem 1.6rem 0 0',
              img: {
                width: '100%',
                height: 'auto',
              },
            }}>
            <Image
              src={getStatic('images/home-privilege/dummy-thumbnail.png')}
              srcSet={getStatic('images/home-privilege/dummy-thumbnail@2x.png')}
            />
          </div>
          <div
            css={{
              padding: '2rem',
            }}>
            <div
              css={{
                fontSize: '3rem',
                color: '#fff',
                ...(isLightTheme && {
                  color: '#000000',
                }),
                [media('xl')]: {
                  fontSize: '2.8rem',
                },
              }}>
              แพ็กเกจประกันภัย convini
            </div>
            <div
              css={{
                fontSize: '2.3rem',
                color: '#A1A1A6',
                ...(isLightTheme && {
                  color: '#86868B',
                }),
                [media('xl')]: {
                  fontSize: '2.2rem',
                },
              }}>
              ประกันที่พร้อมจะดูแลคุณในทุกๆการเดินทาง
            </div>
            <div>
              <ButtonMore tagName="div">ดูเพิ่มเติม</ButtonMore>
            </div>
          </div>
        </a>
      </CustomLink>
    </div>
  )
}
