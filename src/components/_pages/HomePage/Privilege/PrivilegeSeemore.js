import { useTheme } from '@emotion/react'
import { getStatic } from '@lib/static'
import CustomLink from '@link'

export default function PrivilegeSeemore() {
  const { variables } = useTheme()
  return (
    <div>
      <CustomLink route="privilegeList" passHref>
        <a
          css={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: '2.4rem',
            color: variables.colors.highlight.lvl1,
          }}>
          <div
            css={{
              marginRight: '1rem',
            }}>
            สิทธิประโยชน์ทั้งหมด
          </div>
          <img src={getStatic('images/home-privilege/icon-chevron-down.svg')} />
        </a>
      </CustomLink>
    </div>
  )
}
