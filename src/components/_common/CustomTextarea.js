export default function CustomTextarea({ register, ...props }) {
  return (
    <textarea
      ref={register}
      css={{
        width: '100%',
        fontSize: '2.2rem',
        border: '1px solid #BCBDC1',
        borderRadius: '.8rem',
        display: 'flex',
        alignItems: 'center',
        outline: 'none',
        textIndent: '1.5rem',
        backgroundColor: '#fff',
        minHeight: '13.6rem',
        '&:focus': {
          outline: 'none',
          borderColor: '#333333',
        },
        '&::placeholder': {
          color: '#9B9B9B',
        },
      }}
      {...props}
    />
  )
}
