import { useEffect } from 'react'
export default function Image({
  src,
  srcSet,
  onLoaded = () => null,
  onError = () => null,
  ...restProps
}) {
  const attrs = {
    src,
    ...(srcSet && { srcSet: srcSet + ' 2x' }),
  }

  useEffect(() => {
    const img = document.createElement('IMG')
    img.src = srcSet || src
    function loaded() {
      onLoaded()
    }
    function loadError() {
      onError()
    }
    if (img.complete) {
      loaded()
    } else {
      img.addEventListener('load', loaded)
      img.addEventListener('error', loadError)
      return () => {
        img.removeEventListener('load', loaded)
        img.removeEventListener('error', loadError)
      }
    }
  }, [src, srcSet])

  return (
    <img
      css={{
        display: 'block',
      }}
      {...attrs}
      {...restProps}
    />
  )
}
