import { forwardRef } from 'react'
import { useTheme } from '@emotion/react'
import { darken } from 'polished'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'
import CustomLink from '@link'
function ButtonPrevInner({
  tagName = 'a',
  forwardRef,
  children,
  ...restProps
}) {
  const { variables } = useTheme()
  const TagName = tagName
  return (
    <TagName
      ref={forwardRef}
      css={{
        lineHeight: '1',
        fontSize: '2.4rem',
        color: '#6A6B70',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0.5rem 1rem',
        height: '4.6rem',
        minWidth: '12.2rem',
        '&:active': {
          color: darken(0.2, variables.colors.highlight.lvl1),
        },
        [media('xl')]: {
          '&:hover': {
            color: darken(0.2, variables.colors.highlight.lvl1),
          },
        },
      }}
      {...restProps}>
      <img
        css={{
          display: 'block',
          marginRight: '1.5rem',
          height: '1.3rem',
          width: 'auto',
        }}
        width="8"
        height="13"
        src={getStatic('images/icon-chevron-left.svg')}
      />
      {children}
    </TagName>
  )
}

const ButtonPrevInnerWithLink = forwardRef((props, ref) => (
  <ButtonPrevInner {...props} tagName="a" forwardRef={ref} />
))

export default function ButtonPrev({ route, ...restProps }) {
  if (route) {
    return (
      <CustomLink route={route} passHref>
        <ButtonPrevInnerWithLink {...restProps} />
      </CustomLink>
    )
  }
  return <ButtonPrevInner {...restProps} />
}
