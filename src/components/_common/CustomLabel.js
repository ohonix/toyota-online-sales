export default function CustomLabel({ children, required }) {
  return (
    <label
      css={{
        color: '#000000',
        fontSize: '2rem',
        display: 'block',
        marginBottom: '.5rem',
        lineHeight: '1',
      }}>
      {children || 'label'}
      {required && (
        <span css={{ color: '#ED1B2F', marginLeft: '.5rem' }}>*</span>
      )}
    </label>
  )
}
