export default function CustomInput({ register, ...props }) {
  return (
    <input
      ref={register}
      css={{
        width: '100%',
        fontSize: '2.2rem',
        height: '4.6rem',
        border: '1px solid #BCBDC1',
        borderRadius: '.8rem',
        display: 'flex',
        alignItems: 'center',
        outline: 'none',
        textIndent: '1.5rem',
        backgroundColor: '#fff',
        '&:focus': {
          outline: 'none',
          borderColor: '#333333',
        },
        '&::placeholder': {
          color: '#9B9B9B',
        },
      }}
      {...props}
    />
  )
}
