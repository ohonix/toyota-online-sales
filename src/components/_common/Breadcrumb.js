import CustomLink from '@link'

export default function Breadcrumb({ items = [] }) {
  const itemsTotal = items.length > 0
  if (itemsTotal > 0) {
    return (
      <nav
        css={{
          display: 'flex',
          fontSize: '2rem',
          padding: '2rem 0',
        }}>
        {items.map(({ route, label }, i) => {
          if (route) {
            return (
              <span key={i}>
                <CustomLink route={route} passHref>
                  <a
                    css={{
                      color: '#6A6B70',
                      '&:hover': {
                        color: '#ED1B2F',
                      },
                    }}>
                    {label}
                  </a>
                </CustomLink>
                <span
                  css={{
                    fontSize: '1.4rem',
                    margin: '0 .6rem',
                  }}>
                  &gt;
                </span>
              </span>
            )
          }
          return (
            <span
              key={i}
              css={{
                color: '#ED1B2F',
              }}>
              {label}
            </span>
          )
        })}
      </nav>
    )
  }
  return null
}
