export default function ErrorFormMessage({ children }) {
  return (
    <div
      css={{
        fontSize: '1.8rem',
        color: '#ED1B2F',
      }}>
      {children}
    </div>
  )
}
