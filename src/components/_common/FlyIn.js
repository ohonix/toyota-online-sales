import { useSpring, animated } from 'react-spring'
export default function FlyIn({ children, directionTo = 'bottom', delay = 0 }) {
  const stylesDirectionFrom = {
    bottom: {
      x: 0,
      y: -20,
    },
    top: {
      x: 0,
      y: 20,
    },
    left: {
      x: 20,
      y: 0,
    },
    right: {
      x: -20,
      y: 0,
    },
  }[directionTo]

  const styles = useSpring({
    from: {
      opacity: 0,
      ...stylesDirectionFrom,
    },
    to: {
      opacity: 1,
      x: 0,
      y: 0,
    },
    delay,
  })

  return <animated.div style={styles}>{children}</animated.div>
}
