import { useTheme } from '@emotion/react'
import { getStatic } from '@lib/static'
export default function ButtonClose({ closeModal }) {
  const { variables } = useTheme()
  return (
    <div
      onClick={closeModal}
      css={{
        cursor: 'pointer',
        position: 'absolute',
        right: '2rem',
        bottom: '100%',
        marginBottom: '1rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        lineHeight: '1',
        color: '#fff',
        '&:hover': {
          color: variables.colors.highlight.lvl1,
        },
        img: {
          width: '1.6rem',
          height: '1.6rem',
          display: 'block',
        },
      }}>
      <div>
        <img
          width="16"
          height="16"
          src={getStatic('images/modal/icon-close.svg')}
        />
      </div>
      <div
        css={{
          fontSize: '2.6rem',
          marginLeft: '.5rem',
        }}>
        ปิด
      </div>
    </div>
  )
}
