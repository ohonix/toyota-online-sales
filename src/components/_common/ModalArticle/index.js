import { Global } from '@emotion/react'
import { media } from '@lib/styles/helpers'
import Modal from 'react-modal'
import ButtonClose from './ButtonClose'

const customStyles = {
  content: {
    top: '10rem',
    left: '0',
    right: '0',
    bottom: '0',
    overflow: 'visible',
  },
  overlay: {
    backgroundColor: `rgba(0, 0, 0, 0.55)`,
  },
}

export default function ModalArticle({
  children,
  closeModal,
  footer,
  isOpen,
  ...restProps
}) {
  return (
    <div>
      <Global
        styles={{
          '.ReactModal__Content': {
            [media('md')]: {
              ...(isOpen && {
                top: '5rem !important',
                bottom: '3rem !important',
              }),
            },
          },
        }}
      />
      <Modal
        style={customStyles}
        closeTimeoutMS={300}
        isOpen={isOpen}
        {...restProps}>
        <ButtonClose closeModal={closeModal} />
        <div
          css={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
          }}>
          <div
            css={{
              height: '100%',
              overflow: 'auto',
              paddingBottom: '2rem',
            }}>
            {children}
          </div>
          {footer && (
            <div css={{ borderTop: '1px solid #D9D9D9', paddingTop: '2rem' }}>
              {footer}
            </div>
          )}
        </div>
      </Modal>
    </div>
  )
}
