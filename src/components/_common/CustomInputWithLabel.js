import CustomInput from './CustomInput'
import CustomLabel from './CustomLabel'

export default function CustomInputWithLabel({
  label,
  required,
  ...restProps
}) {
  return (
    <>
      <CustomLabel required={required}>{label}</CustomLabel>
      <CustomInput {...restProps} />
    </>
  )
}
