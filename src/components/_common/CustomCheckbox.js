import { getStatic } from '@lib/static'

export default function CustomCheckbox({ children, register, name }) {
  return (
    <div>
      <label
        css={{
          cursor: 'pointer',
          display: 'inline-flex',
          '.icon': {
            img: {
              opacity: 0,
              transform: 'scale(0)',
            },
          },
          'input:checked ~ .icon': {
            img: {
              opacity: 1,
              transform: 'scale(1)',
            },
          },
        }}>
        <input
          name={name}
          ref={register}
          css={{
            display: 'none',
          }}
          type="checkbox"
        />
        <div
          className="icon"
          css={{
            width: '3rem',
            height: '3rem',
            flex: '0 0 3rem',
            borderRadius: '.4rem',
            border: '1px solid #CCCCCC',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: '1rem',
          }}>
          <img
            css={{
              width: 'auto',
              height: '1.1rem',
              transition: 'all 0.3s',
            }}
            src={getStatic('images/icon-checked.svg')}
          />
        </div>
        <div
          css={{
            width: '100%',
            color: '#000000',
            paddingTop: '.3rem',
          }}>
          {children}
        </div>
      </label>
    </div>
  )
}
