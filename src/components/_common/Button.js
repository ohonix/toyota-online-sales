import { forwardRef } from 'react'
import { useTheme } from '@emotion/react'
import { darken } from 'polished'
import { media } from '@lib/styles/helpers'
import CustomLink from '@link'
function ButtonInner({
  tagName = 'button',
  children,
  disabled = false,
  ...restProps
}) {
  const { variables } = useTheme()
  const TagName = tagName
  return (
    <TagName
      css={{
        border: 'none',
        outline: 'none !important',
        cursor: 'pointer',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '4.6rem',
        minWidth: '12.2rem',
        padding: '0.5rem 2rem',
        lineHeight: 1,
        fontSize: '2.4rem',
        color: '#fff',
        backgroundColor: variables.colors.highlight.lvl1,
        borderRadius: '9.9rem',
        '&:active': {
          backgroundColor: darken(0.2, variables.colors.highlight.lvl1),
        },
        ...(disabled && {
          opacity: 0.5,
          pointerEvents: 'none',
        }),
        [media('xl')]: {
          height: '4.8rem',
          padding: '0.5rem 3rem',
          minWidth: '17rem',
          '&:hover': {
            backgroundColor: darken(0.2, variables.colors.highlight.lvl1),
          },
        },
      }}
      {...restProps}>
      {children}
    </TagName>
  )
}

const ButtonWithLink = forwardRef((props, ref) => (
  <ButtonInner {...props} tagName="a" forwardRef={ref} />
))

export default function Button({ route, ...restProps }) {
  if (route) {
    return (
      <CustomLink route={route} passHref>
        <ButtonWithLink {...restProps} />
      </CustomLink>
    )
  }
  return <ButtonInner {...restProps} />
}
