import { getStatic } from '@lib/static'

export default function CustomCheckboxAgreement({
  children,
  register,
  ref,
  name,
  ...restProps
}) {
  return (
    <div
      css={{
        display: 'flex',
        '.icon': {
          img: {
            opacity: 0,
            transform: 'scale(0)',
          },
        },
        'label input:checked ~ .icon': {
          img: {
            opacity: 1,
            transform: 'scale(1)',
          },
        },
      }}>
      <label
        css={{
          cursor: 'pointer',
          flex: '0 0 3rem',
          marginRight: '1rem',
        }}>
        <input
          name={name}
          ref={ref || register}
          css={{
            display: 'none',
          }}
          type="checkbox"
          {...restProps}
        />
        <div
          className="icon"
          css={{
            width: '3rem',
            height: '3rem',
            borderRadius: '.4rem',
            border: '1px solid #CCCCCC',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <img
            css={{
              width: 'auto',
              height: '1.1rem',
              transition: 'all 0.3s',
            }}
            src={getStatic('images/icon-checked.svg')}
          />
        </div>
      </label>
      <div
        css={{
          width: '100%',
          color: '#000000',
          fontSize: '1.8rem',
          paddingTop: '.5rem',
          a: {
            color: '#6A6B70',
            textDecoration: 'underline',
          },
        }}>
        {children}
      </div>
    </div>
  )
}
