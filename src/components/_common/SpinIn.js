import { useSpring, animated } from 'react-spring'
export default function SpinIn({ children, delay = 0, reverseRotate = false }) {
  const styles = useSpring({
    from: {
      opacity: 0,
      scale: 0,
      rotate: reverseRotate ? -360 : 360,
    },
    to: {
      opacity: 1,
      scale: 1,
      rotate: 0,
    },
    reset: true,
    delay,
  })

  return <animated.div style={styles}>{children}</animated.div>
}
