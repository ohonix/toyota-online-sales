import Modal from 'react-modal'
import ButtonClose from './ButtonClose'

const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    overflow: 'visible',
    position: 'relative',
    margin: 'auto',
  },
  overlay: {
    backgroundColor: `rgba(0, 0, 0, 0.55)`,
    display: 'flex',
    padding: '3rem 1.5rem',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
}

export default function ModalMessage({
  children,
  closeModal,
  isOpen,
  ...restProps
}) {
  return (
    <div>
      <Modal
        style={customStyles}
        closeTimeoutMS={300}
        isOpen={isOpen}
        {...restProps}>
        <ButtonClose closeModal={closeModal} />
        {children}
      </Modal>
    </div>
  )
}
