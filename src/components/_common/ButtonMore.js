import { forwardRef } from 'react'
import { useTheme } from '@emotion/react'
import { darken } from 'polished'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'
import CustomLink from '@link'
function ButtonMoreInner({
  tagName = 'a',
  forwardRef,
  children,
  ...restProps
}) {
  const { variables } = useTheme()
  const TagName = tagName
  return (
    <TagName
      ref={forwardRef}
      css={{
        lineHeight: '1',
        fontSize: '2.4rem',
        color: variables.colors.highlight.lvl1,
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0.5rem 1rem',
        height: '4.6rem',
        minWidth: '12.2rem',
        '&:active': {
          color: darken(0.2, variables.colors.highlight.lvl1),
        },
        [media('xl')]: {
          '&:hover': {
            color: darken(0.2, variables.colors.highlight.lvl1),
          },
        },
      }}
      {...restProps}>
      {children}
      <img
        css={{
          display: 'block',
          marginLeft: '0.7rem',
        }}
        width="6"
        height="9"
        src={getStatic('images/home-highlight/icon-navigate-next.svg')}
      />
    </TagName>
  )
}

const ButtonMoreInnerWithLink = forwardRef((props, ref) => (
  <ButtonMoreInner {...props} tagName="a" forwardRef={ref} />
))

export default function ButtonMore({ route, ...restProps }) {
  if (route) {
    return (
      <CustomLink route={route} passHref>
        <ButtonMoreInnerWithLink {...restProps} />
      </CustomLink>
    )
  }
  return <ButtonMoreInner {...restProps} />
}
