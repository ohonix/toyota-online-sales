import { Controller } from 'react-hook-form'
import Select from 'react-select'

function setSelectStylesControl(provided, { isFocused }) {
  return {
    ...provided,
    border: '1px solid #BCBDC1',
    height: '4.6rem',
    borderRadius: '.8rem',
    backgroundColor: '#fff',
    ...(isFocused && {
      borderColor: '#000  !important',
      boxShadow: '#000 !important',
    }),
  }
}
function setPlaceholder(provided) {
  return {
    ...provided,
    fontSize: '2.2rem',
    color: '#9B9B9B',
    paddingLeft: '1rem',
    paddingRight: '1rem',
  }
}
function setInput(provided) {
  return {
    ...provided,
    marginLeft: '1rem',
    color: '#000000',
  }
}
function setSelectSingleValue(provided) {
  return {
    ...provided,
    color: '#000000',
    marginLeft: '1rem',
  }
}
function setSelectStylesIndicatorSeparator(provided) {
  return {
    display: 'none',
  }
}
function setSelectStylesIndicatorsContainer(provided) {
  return {
    ...provided,
    svg: {
      fill: '#000',
    },
  }
}
function setSelectStylesOption(provided, { isSelected, isFocused }) {
  return {
    ...provided,
    ...(isFocused && {
      backgroundColor: 'rgba(237, 27, 47, 0.2) !important',
      color: '#000 !important',
    }),
    ...(isSelected && {
      backgroundColor: '#ED1B2F !important',
      color: '#fff !important',
    }),
  }
}

export default function CustomSelect({ control, name, ...props }) {
  const InstanceSelect = props => (
    <Select
      {...props}
      styles={{
        control: setSelectStylesControl,
        placeholder: setPlaceholder,
        singleValue: setSelectSingleValue,
        input: setInput,
        indicatorSeparator: setSelectStylesIndicatorSeparator,
        indicatorsContainer: setSelectStylesIndicatorsContainer,
        option: setSelectStylesOption,
      }}
    />
  )
  if (control) {
    return (
      <div>
        <Controller
          name={name}
          control={control}
          render={controllerProps => {
            return <InstanceSelect {...props} {...controllerProps} />
          }}
        />
      </div>
    )
  }
  return <InstanceSelect {...props} />
}
