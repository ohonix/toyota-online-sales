import { useState, useEffect } from 'react'
import { useTheme } from '@emotion/react'
import { darken } from 'polished'

import { Container } from '@grid'
import { getStatic } from '@lib/static'
import { media, Responsive } from '@lib/styles/helpers'
import Image from '@components/_common/Image'
import FlyIn from '@components/_common/FlyIn'
import Switcher from './Switcher'
import Languages from '../Navigation/Languages'
import NavigationMobileContainer from '../Navigation/NavigationMobileContainer'
import NavigationDesktopContainer from '../Navigation/NavigationDesktopContainer'
import CustomLink from '@link'

export default function Header() {
  const [navIsOpen, setNavIsOpen] = useState()
  function toggleNavigation() {
    setNavIsOpen(navIsOpen => !navIsOpen)
  }
  const { variables } = useTheme()
  useEffect(() => {
    const className = 'mobile-nav-opened'
    if (navIsOpen) {
      window.document.documentElement.classList.add(className)
    } else {
      window.document.documentElement.classList.remove(className)
    }
  }, [navIsOpen])
  return (
    <div
      css={{
        position: 'relative',
        zIndex: variables.zIndex.header,
      }}>
      <div
        css={{
          background: '#000',
          color: '#fff',
          position: 'relative',
          zIndex: 1,
        }}>
        <Container>
          <div
            css={{
              padding: '1rem 0',
              margin: '0 -.5rem',
              display: 'flex',
              alignItems: 'center',
              [media('sm')]: { margin: 0 },
            }}>
            <FlyIn>
              <CustomLink route="home" passHref>
                <a>
                  <Image
                    width="114"
                    height="36"
                    src={getStatic('images/header/logo-toyota.png')}
                    srcSet={getStatic('images/header/logo-toyota@2x.png')}
                  />
                </a>
              </CustomLink>
            </FlyIn>
            <div
              css={{
                marginLeft: '8rem',
              }}>
              <Responsive
                breakpoint="xl"
                desktop={
                  <FlyIn>
                    <NavigationDesktopContainer />
                  </FlyIn>
                }
              />
            </div>
            <div
              css={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: 'auto',
              }}>
              <FlyIn delay={400}>
                <a
                  css={{
                    fontSize: '2.2rem',
                    color: '#FFFFFF',
                    backgroundColor: variables.colors.highlight.lvl1,
                    borderRadius: '9.9rem',
                    display: 'inline-flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    minWidth: '8.2rem',
                    height: '4rem',
                    padding: '0.6rem',
                    cursor: 'pointer',
                    '&:active': {
                      backgroundColor: darken(
                        0.2,
                        variables.colors.highlight.lvl1,
                      ),
                    },
                    [media('xl')]: {
                      '&:hover': {
                        backgroundColor: darken(
                          0.2,
                          variables.colors.highlight.lvl1,
                        ),
                      },
                    },
                  }}>
                  <div>ซื้อรถ</div>
                </a>
              </FlyIn>
              <div
                css={{
                  padding: '1rem',
                  marginLeft: '1.5rem',
                }}>
                <FlyIn delay={600}>
                  <a
                    css={{
                      display: 'inline-flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      cursor: 'pointer',
                      color: '#fff',
                      '&:active': {
                        color: variables.colors.highlight.lvl1,
                      },
                      [media('xl')]: {
                        '&:hover': {
                          color: variables.colors.highlight.lvl1,
                        },
                      },
                    }}>
                    <img
                      css={{
                        display: 'block',
                        maxWidth: 'none',
                        width: '1.4rem',
                        height: '1.6rem',
                      }}
                      src={getStatic('images/header/icon-user.svg')}
                    />
                    <Responsive
                      breakpoint="xl"
                      desktop={
                        <div css={{ marginLeft: '1rem' }}>เข้าสู่ระบบ</div>
                      }
                    />
                  </a>
                </FlyIn>
              </div>
              <div
                css={{
                  marginLeft: '1rem',
                }}>
                <Responsive
                  breakpoint="xl"
                  desktop={
                    <FlyIn delay={800}>
                      <Languages />
                    </FlyIn>
                  }
                />
              </div>
              <Responsive
                breakpoint="xl"
                mobile={
                  <Switcher
                    toggleNavigation={toggleNavigation}
                    navIsOpen={navIsOpen}
                  />
                }
              />
            </div>
          </div>
        </Container>
      </div>
      <Responsive
        breakpoint="xl"
        mobile={<NavigationMobileContainer navIsOpen={navIsOpen} />}
      />
    </div>
  )
}
