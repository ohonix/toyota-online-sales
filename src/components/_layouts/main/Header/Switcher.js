import { getStatic } from '@lib/static'
import FlyIn from '@components/_common/FlyIn'
export default function Switcher({ toggleNavigation, navIsOpen }) {
  return (
    <div
      onClick={toggleNavigation}
      css={{
        marginRight: '-1rem',
        padding: '1rem',
        marginLeft: '.5rem',
      }}>
      <FlyIn delay={800}>
        <div css={{ position: 'relative' }}>
          <img
            css={{
              display: 'block',
              transition: 'all 0.3s',
              width: '1.8rem',
              height: '1.6rem',
              maxWidth: 'none',
              ...(navIsOpen && {
                opacity: 0,
                transform: 'translateX(-1rem) scale(0)',
              }),
            }}
            src={getStatic('images/header/icon-hamburger.svg')}
          />
          <img
            css={{
              display: 'block',
              position: 'absolute',
              right: 0,
              top: 0,
              bottom: 0,
              margin: 'auto',
              transition: 'all 0.3s',
              opacity: 0,
              transform: 'scale(0)',
              width: '1.6rem',
              height: '1.6rem',
              maxWidth: 'none',
              ...(navIsOpen && {
                opacity: 1,
                transform: 'scale(1)',
              }),
            }}
            src={getStatic('images/header/icon-close-menu.svg')}
          />
        </div>
      </FlyIn>
    </div>
  )
}
