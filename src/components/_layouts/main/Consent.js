import { useEffect, useState } from 'react'
import { useTheme } from '@emotion/react'
import { Container } from '@grid'
import { getLocalStorage, setLocalStorage } from '@modules/localStorage'

import Button from '@components/_common/Button'
import { media } from '@lib/styles/helpers'

export default function Consent() {
  const { variables } = useTheme()
  const [alreadyAccepted, setAlreadyAccepted] = useState(true)
  const LOCAL_STORAGE = {
    key: 'consent_cookie',
    value: 'yes',
  }

  useEffect(() => {
    setAlreadyAccepted(
      getLocalStorage(LOCAL_STORAGE.key) === LOCAL_STORAGE.value,
    )
  }, [])

  function handleAccepted() {
    if (!alreadyAccepted) {
      const { key, value } = LOCAL_STORAGE

      setLocalStorage(key, value)
      setAlreadyAccepted(true)
    }
  }

  if (alreadyAccepted === true) return null

  return (
    <div
      css={{
        zIndex: variables.zIndex.consent,
        position: 'fixed',
        left: 0,
        right: 0,
        bottom: 0,
        padding: '1.6rem 0',
        backgroundColor: '#fff',
        color: '#000',
        textAlign: 'center',
        fontSize: '1.8rem',
        [media('lg')]: {
          textAlign: 'left',
          maxWidth: '114rem',
          marginLeft: 'auto',
          marginRight: 'auto',
          backgroundColor: '#F2F2F2',
          padding: '3rem',
          fontSize: '2.4rem',
        },
      }}>
      <Container>
        <div
          css={{
            [media('lg')]: {
              display: 'flex',
              alignItems: 'center',
            },
          }}>
          <div
            css={{
              marginBottom: '1.6rem',
              [media('lg')]: {
                marginBottom: 0,
              },
            }}>
            เว็บไซต์นี้ใช้คุกกี้ในการเก็บข้อมูลการใช้งาน เพื่อให้คุณได้รับ
            ประสบการณ์ที่ดี หากคุณใช้งานเว็บไซต์ต่อเราถือว่าคุณยอมรับ <br />
            <u>การใช้งานคุกกี้ และนโยบายความเป็นส่วนตัว</u>
          </div>
          <div
            css={{
              [media('lg')]: {
                marginLeft: 'auto',
              },
            }}>
            <Button
              tagName="div"
              onClick={handleAccepted}
              css={{
                [media('lg')]: {
                  minWidth: '19.4rem',
                },
              }}>
              ยอมรับ
            </Button>
          </div>
        </div>
      </Container>
    </div>
  )
}
