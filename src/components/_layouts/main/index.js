import Header from './Header'
import Footer from './Footer'
import Consent from './Consent'
import StickyShortcutIcons from './StickyShortcutIcons'

export default function MainLayout({ children }) {
  return (
    <div>
      <Header />
      <main>{children}</main>
      <Footer />
      <Consent />
      <StickyShortcutIcons />
    </div>
  )
}
