export default [
  {
    name: 'แต่งรถ',
    route: 'customize',
  },
  {
    name: 'ชุดแต่ง',
    route: 'home',
  },
  {
    name: 'วิธีการสั่งซื้อ',
    route: 'home',
  },
  {
    name: 'คำนวณสินเชื่อ',
    route: 'home',
  },
  {
    name: 'ติดต่อเรา',
    route: 'contact',
  },
]
