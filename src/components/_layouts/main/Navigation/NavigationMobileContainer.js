import Navigation from './Navigation'
import Contact from './Contact'
import { Container } from '@grid'
export default function NavigationMobileContainer({ navIsOpen }) {
  return (
    <div
      css={{
        position: 'absolute',
        zIndex: 0,
        top: '100%',
        left: 0,
        right: 0,
        backgroundColor: '#191919',
        opacity: 0,
        visibility: 'hidden',
        transform: 'translateY(-2rem)',
        transition: 'all 0.3s',
        ...(navIsOpen && {
          opacity: 1,
          visibility: 'visible',
          transform: 'translateY(0)',
        }),
      }}>
      <Container>
        <Navigation />
      </Container>
      <Contact />
    </div>
  )
}
