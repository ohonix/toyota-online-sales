import { getStatic } from '@lib/static'

const items = [
  {
    name: 'facebook',
    link: 'https://www.toyota.co.th/',
    image: 'images/header/icon-social-facebook.svg',
  },
  {
    name: 'youtube',
    link: 'https://www.toyota.co.th/',
    image: 'images/header/icon-social-youtube.svg',
  },
  {
    name: 'line',
    link: 'https://www.toyota.co.th/',
    image: 'images/header/icon-social-line.svg',
  },
  {
    name: 'instagram',
    link: 'https://www.toyota.co.th/',
    image: 'images/header/icon-social-ig.svg',
  },
]

export default function FollowUs() {
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
      }}>
      <div
        css={{
          marginRight: '2rem',
        }}>
        Find us on
      </div>
      <div
        css={{
          display: 'flex',
          alignItems: 'center',
          a: {
            display: 'block',
            padding: '0 .7rem',
          },
        }}>
        {items.map((item, i) => (
          <div key={i}>
            <a href={item.link} target="_blank" rel="noreferrer">
              <img src={getStatic(item.image)} alt={item.name} />
            </a>
          </div>
        ))}
      </div>
    </div>
  )
}
