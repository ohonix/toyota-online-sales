import { forwardRef } from 'react'
import CustomLink from '@link'
import menuItems from './menuItems'
import { media } from '@lib/styles/helpers'
import { useTheme } from '@emotion/react'

export default function Navigation() {
  const { variables } = useTheme()
  const LinkItem = forwardRef(({ menu, ...props }, ref) => (
    <a
      {...props}
      ref={ref}
      css={{
        display: 'block',
        color: '#fff',
        fontSize: '2.6rem',
        borderBottom: '1px solid #252525',
        cursor: 'pointer',
        '&:active': {
          color: variables.colors.highlight.lvl1,
        },
        [media('xl')]: {
          fontSize: '2.2rem',
          borderBottom: 'none',
          marginRight: '1rem',
          '&:hover': {
            color: variables.colors.highlight.lvl1,
          },
        },
      }}>
      <span
        css={{
          display: 'block',
          padding: '2rem',
          [media('xl')]: {
            padding: '1rem',
          },
        }}>
        {menu.name}
      </span>
    </a>
  ))

  return (
    <nav>
      {menuItems.map(menu => (
        <CustomLink key={menu.name} route={menu.route} passHref>
          <LinkItem menu={menu} />
        </CustomLink>
      ))}
    </nav>
  )
}
