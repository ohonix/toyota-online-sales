import { Container } from '@grid'
import FollowUs from './FollowUs'
import Languages from './Languages'

export default function Contact() {
  return (
    <div
      css={{
        backgroundColor: '#333333',
        color: '#FFFFFF',
        padding: '2rem 0',
      }}>
      <Container>
        <div
          css={{
            fontSize: '3.2rem',
            fontFamily: 'db_helvethaica_x65_med',
            letterSpacing: '.2rem',
          }}>
          ศูนย์บริการข้อมูลลูกค้า
        </div>
        <div>
          <span
            css={{
              fontSize: '3.6rem',
              textDecoration: 'underline',
              fontFamily: 'db_helvethaica_x65_med',
              color: '#FFFFFF',
              marginRight: '0.5rem',
            }}>
            (66) 0-2386-2000
          </span>{' '}
          <span css={{ fontSize: '2.2rem', color: '#ED1B2F' }}>
            ตลอด 24 ชั่วโมง
          </span>
        </div>
        <div
          css={{
            fontSize: '2.2rem',
            color: '#9B9B9B',
            marginBottom: '3rem',
            a: {
              color: '#9B9B9B',
            },
          }}>
          <a>comtactcenter@toyota.co.th</a>
        </div>
        <div
          css={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <FollowUs />
          <Languages />
        </div>
      </Container>
    </div>
  )
}
