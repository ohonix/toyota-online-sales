import Navigation from './Navigation'
export default function NavigationDesktopContainer() {
  return (
    <div
      css={{
        nav: {
          display: 'flex',
          alignItems: 'center',
        },
      }}>
      <Navigation />
    </div>
  )
}
