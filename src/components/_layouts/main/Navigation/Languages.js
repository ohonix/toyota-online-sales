import { media } from '@lib/styles/helpers'

export default function Languages() {
  return (
    <div
      css={{
        fontSize: '2rem',
        lineHeight: 1,
        '> *': {
          display: 'inline-flex',
        },
        '> a': {
          padding: '1rem',
        },
        [media('xl')]: {
          fontSize: '2.2rem',
        },
      }}>
      <a
        css={{
          color: '#ED1B2F',
        }}>
        ไทย
      </a>
      <span>|</span>
      <a
        css={{
          color: '#ffffff',
        }}>
        EN
      </a>
    </div>
  )
}
