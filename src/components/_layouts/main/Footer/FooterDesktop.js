import Image from '@components/_common/Image'
import { useTheme } from '@emotion/react'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'
import Copyright from './Copyright'
import FollowUs from './FollowUs'
import Navs from './Navs'
import NewsletterButton from './NewsletterButton'
import SubNavs from './SubNavs'
export default function FooterDesktop() {
  const { variables } = useTheme()
  return (
    <div>
      <div
        css={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderBottom: '1px solid #D6D6D6',
          paddingBottom: '2rem',
          marginBottom: '2rem',
        }}>
        <FollowUs />
        <Navs />
        <NewsletterButton />
      </div>
      <div
        css={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <div>
          <Image
            width="85"
            height="34"
            src={getStatic('images/footer/icon-dbd.png')}
            srcSet={getStatic('images/footer/icon-dbd@2x.png')}
          />
        </div>
        <div
          css={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <div
            css={{
              order: 1,
            }}>
            <SubNavs />
          </div>
          <Copyright />
        </div>
        <div
          css={{
            display: 'flex',
            fontSize: '1.8rem',
          }}>
          <div
            css={{
              fontWeight: 'bold',
              color: '#1D1D1F',
              fontFamily: 'db_helvethaica_x75_bd',
            }}>
            ไทย
          </div>
          <div
            css={{
              margin: '0 .5rem',
            }}>
            |
          </div>
          <div
            css={{
              color: '#86868B',
              cursor: 'pointer',
              '&:active': {
                color: variables.colors.highlight.lvl1,
              },
              [media('xl')]: {
                '&:hover': {
                  color: variables.colors.highlight.lvl1,
                },
              },
            }}>
            EN
          </div>
        </div>
      </div>
    </div>
  )
}
