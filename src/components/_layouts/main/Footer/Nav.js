import { useTheme } from '@emotion/react'
import { media } from '@lib/styles/helpers'

export default function Nav({ children }) {
  const { variables } = useTheme()
  return (
    <div
      css={{
        borderBottom: '1px solid #909095',
        [media('xl')]: {
          borderBottom: 'none',
        },
        a: {
          display: 'block',
          color: '#000000',
          fontSize: '2.2rem',
          '&:active': {
            color: variables.colors.highlight.lvl1,
          },
          [media('xl')]: {
            paddingLeft: '1rem',
            paddingRight: '1rem',
            marginLeft: '.2rem',
            marginRight: '.2rem',
            '&:hover': {
              color: variables.colors.highlight.lvl1,
            },
          },
        },
        '> *': {
          paddingTop: '1.5rem',
          paddingBottom: '1.5rem',
        },
      }}>
      {children}
    </div>
  )
}
