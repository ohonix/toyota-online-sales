import { media, Responsive } from '@lib/styles/helpers'
import FollowUs from './FollowUs'
import Nav from './Nav'
import menuItems from '@components/_layouts/main/Navigation/menuItems'
import CustomLink from '@link'
const itmes = [
  ...menuItems,
  {
    name: 'Link to toyota.co.th ',
    href: 'https://www.toyota.co.th/',
  },
]

export default function Navs() {
  return (
    <div
      css={{
        marginBottom: '1.5rem',
        [media('xl')]: {
          marginBottom: 0,
          display: 'flex',
          alignItems: 'center',
        },
      }}>
      <Responsive
        breakpoint="xl"
        mobile={
          <Nav>
            <FollowUs />
          </Nav>
        }
      />
      {itmes.map(({ route, name, href }, i) => (
        <Nav key={i}>
          {route ? (
            <CustomLink route={route} passHref>
              <a>{name}</a>
            </CustomLink>
          ) : (
            <a href={href} target="_blank" rel="noreferrer">
              {name}
            </a>
          )}
        </Nav>
      ))}
    </div>
  )
}
