import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

const items = [
  {
    name: 'facebook',
    link: 'https://www.toyota.co.th/',
    image: 'images/footer/icon-social-facbook.svg',
  },
  {
    name: 'youtube',
    link: 'https://www.toyota.co.th/',
    image: 'images/footer/icon-social-youtube.svg',
  },
  {
    name: 'line',
    link: 'https://www.toyota.co.th/',
    image: 'images/footer/icon-social-line.svg',
  },
  {
    name: 'instagram',
    link: 'https://www.toyota.co.th/',
    image: 'images/footer/icon-social-ig.svg',
  },
]
export default function FollowUs() {
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
      }}>
      <div
        css={{
          marginRight: '4rem',
          [media('xl')]: {
            marginRight: '1.6rem',
          },
        }}>
        Find us on
      </div>
      <div
        css={{
          display: 'flex',
          alignItems: 'center',
          a: {
            display: 'block',
            padding: '0 1rem',
            img: {
              display: 'block',
              transition: 'all 0.3s',
            },
            '&:active': {
              img: {
                transform: 'scale(1.1)',
              },
            },
            [media('xl')]: {
              '&:hover': {
                img: {
                  transform: 'scale(1.1)',
                },
              },
            },
          },
        }}>
        {items.map((item, i) => (
          <div key={i}>
            <a href={item.link} target="_blank" rel="noreferrer">
              <img src={getStatic(item.image)} alt={item.name} />
            </a>
          </div>
        ))}
      </div>
    </div>
  )
}
