import { useTheme } from '@emotion/react'
import { getStatic } from '@lib/static'
import { media } from '@lib/styles/helpers'

export default function NewsletterButton() {
  const { variables } = useTheme()
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '10rem',
        boxShadow: '0 .2rem .4rem rgba(0, 0, 0, 0.08)',
        width: '100%',
        height: '5rem',
        backgroundColor: '#fff',
        padding: '.5rem 1.6rem',
        cursor: 'pointer',
        color: '#000',
        fontSize: '2.2rem',
        marginTop: '2rem',
        marginBottom: '3rem',
        '.isText': {
          color: '#000',
        },
        '&:active': {
          '.isText': {
            color: variables.colors.highlight.lvl1,
          },
        },
        [media('xl')]: {
          borderRadius: '2rem',
          marginBottom: 0,
          height: '3.8rem',
          fontSize: '1.8rem',
          display: 'inline-flex',
          width: 'auto',
          '&:hover': {
            '.isText': {
              color: variables.colors.highlight.lvl1,
            },
          },
        },
      }}>
      <div
        css={{
          marginRight: ' 1.2rem',
        }}>
        <img
          css={{
            display: 'block',
            width: '2rem',
            height: 'auto',
            [media('xl')]: {
              width: '1.7rem',
            },
          }}
          src={getStatic('images/footer/icon-email.svg')}
        />
      </div>
      <div className="isText">รับข่าวสารและข้อเสนอพิเศษ</div>
    </div>
  )
}
