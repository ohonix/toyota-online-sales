import { media } from '@lib/styles/helpers'

export default function Copyright() {
  return (
    <div
      css={{
        fontSize: '1.3rem',
        color: '#86868B',
        textAlign: 'right',
        [media('xl')]: {
          textAlign: 'center',
          fontSize: '1.8rem',
        },
      }}>
      <div
        css={{
          [media('xl')]: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          },
        }}>
        <div>สงวนลิขสิทธิ์ พ.ศ. 2561</div>
        <div>บริษัท โตโยต้า มอเตอร์ ประเทศไทย จำกัด</div>
      </div>
    </div>
  )
}
