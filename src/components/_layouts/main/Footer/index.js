import { Container } from '@grid'
import { Responsive } from '@lib/styles/helpers'
import FooterMobile from './FooterMobile'
import FooterDesktop from './FooterDesktop'

export default function Footer() {
  return (
    <div>
      <div
        css={{
          background: '#F5F5F7',
          color: '#000',
          padding: '1rem 0 2rem',
        }}>
        <Container>
          <Responsive
            breakpoint="xl"
            mobile={<FooterMobile />}
            desktop={<FooterDesktop />}
          />
        </Container>
      </div>
    </div>
  )
}
