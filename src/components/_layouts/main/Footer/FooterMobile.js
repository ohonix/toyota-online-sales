import NewsletterButton from './NewsletterButton'
import Navs from './Navs'
import SubNavs from './SubNavs'
import Copyright from './Copyright'
import Image from '@components/_common/Image'
import { getStatic } from '@lib/static'
export default function FooterMobile() {
  return (
    <div>
      <NewsletterButton />
      <Navs />
      <SubNavs />
      <div
        css={{
          display: 'flex',
          justifyContent: 'space-between',
        }}>
        <div>
          <Image
            css={{
              width: 'auto',
              height: '3.2rem',
            }}
            width="85"
            height="34"
            src={getStatic('images/footer/icon-dbd.png')}
            srcSet={getStatic('images/footer/icon-dbd@2x.png')}
          />
        </div>
        <Copyright />
      </div>
    </div>
  )
}
