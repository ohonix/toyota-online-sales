import { useTheme } from '@emotion/react'
import { media } from '@lib/styles/helpers'
import CustomLink from '@link'

const items = [
  {
    name: 'เงื่อนไขการใช้งาน',
    route: 'policy',
  },
  {
    name: 'นโยบายความเป็นส่วนตัว',
    route: '',
  },
  {
    name: 'นโยบายการใช้คุกกี้',
    route: '',
  },
]
export default function SubNavs() {
  const { variables } = useTheme()
  return (
    <div
      css={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        fontSize: '1.6rem',
        textAlign: 'center',
        marginBottom: '2rem',
        [media('xl')]: {
          marginBottom: '0',
          fontSize: '1.8rem',
          justifyContent: 'center',
          width: 'auto',
        },
        '> *': {
          [media('sm')]: {
            width: '100%',
          },
          [media('xl')]: {
            width: 'auto',
          },
        },
        a: {
          color: '#000',
          display: 'block',
          padding: '0 1rem',
          '&:active': {
            color: variables.colors.highlight.lvl1,
          },
          [media('xl')]: {
            '&:hover': {
              color: variables.colors.highlight.lvl1,
            },
          },
        },
        '> div:not(:last-of-type)': {
          borderRight: '1px solid #000000',
        },
      }}>
      {items.map(({ route, name }, i) => (
        <div key={i}>
          <CustomLink route={route} passHref>
            <a>{name}</a>
          </CustomLink>
        </div>
      ))}
    </div>
  )
}
