import { useTheme } from '@emotion/react'
import { useState } from 'react'
import IconList from './IconList'
import IconListWithText from './IconListWithText'
import Switcher from './Switcher'

export default function StickyShortcutIcons() {
  const [expand, setExpand] = useState(false)
  const { variables } = useTheme()
  function togglerExpand() {
    setExpand(expand => !expand)
  }
  return (
    <div
      css={{
        position: 'fixed',
        right: '1.3rem',
        bottom: '3rem',
        zIndex: variables.zIndex.stickyShortcutIcons,
      }}>
      <div>{expand && <IconListWithText />}</div>
      <Switcher expand={expand} onClick={togglerExpand} />
      <div>{!expand && <IconList />}</div>
    </div>
  )
}
