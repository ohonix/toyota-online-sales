import SpinIn from '@components/_common/SpinIn'
import { getStatic } from '@lib/static'
import Icon from './Icon'

export default function Switcher({ expand, onClick = () => null }) {
  return (
    <div
      onClick={onClick}
      css={{
        width: '4.8rem',
        position: 'relative',
        marginLeft: 'auto',
      }}>
      {!expand ? (
        <SpinIn>
          <div
            css={{
              img: {
                width: '2.4rem',
                height: '.6rem',
              },
            }}>
            <Icon>
              <img
                src={getStatic('images/sticky-shortcut-icon/icon-3dots.svg')}
              />
            </Icon>
          </div>
        </SpinIn>
      ) : (
        <SpinIn>
          <div
            css={{
              img: {
                width: '2.2rem',
                height: '2.2rem',
              },
            }}>
            <Icon>
              <img
                src={getStatic('images/sticky-shortcut-icon/icon-close.svg')}
              />
            </Icon>
          </div>
        </SpinIn>
      )}
    </div>
  )
}
