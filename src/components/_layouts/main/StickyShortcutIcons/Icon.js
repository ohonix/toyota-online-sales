import { media } from '@lib/styles/helpers'

export default function Icon({ children, ...props }) {
  return (
    <div
      css={{
        marginTop: '1rem',
        width: '4.8rem',
        height: '4.8rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        textAlign: 'center',
        cursor: 'pointer',
        '&:after': {
          content: '""',
          display: 'block',
          borderRadius: '100%',
          position: 'absolute',
          zIndex: 0,
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          opacity: 0.5,
          backgroundColor: '#000000',
          border: '1px solid #707070',
          transition: 'all 0.3s',
        },
        img: {
          display: 'block',
          position: 'relative',
          zIndex: 1,
        },
        '&:active': {
          '&:after': {
            opacity: 1,
          },
        },
        [media('xl')]: {
          '&:hover': {
            '&:after': {
              opacity: 1,
            },
          },
        },
      }}
      {...props}>
      {children}
    </div>
  )
}
