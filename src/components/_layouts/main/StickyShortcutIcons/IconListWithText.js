import FlyIn from '@components/_common/FlyIn'
import { getStatic } from '@lib/static'
import IconWithLabel from './IconWithLabel'

export default function IconListWithText() {
  return (
    <>
      <FlyIn directionTo="right" delay={400}>
        <div
          css={{
            img: {
              width: '2.3rem',
              height: '1.5rem',
            },
          }}>
          <IconWithLabel
            label="สอบถาม"
            imgSrc={getStatic('images/sticky-shortcut-icon/icon-email.svg')}
          />
        </div>
      </FlyIn>
      <FlyIn directionTo="right" delay={200}>
        <div
          css={{
            img: {
              width: '2.2rem',
              height: '2.2rem',
            },
          }}>
          <IconWithLabel
            label="คำนวณสินเชื่อ"
            imgSrc={getStatic('images/sticky-shortcut-icon/icon-calc.svg')}
          />
        </div>
      </FlyIn>
      <FlyIn directionTo="right">
        <div
          css={{
            img: {
              width: '2.6rem',
              height: '2.6rem',
            },
          }}>
          <IconWithLabel
            label="ทดลองขับ"
            imgSrc={getStatic(
              'images/sticky-shortcut-icon/icon-steering-wheel.svg',
            )}
          />
        </div>
      </FlyIn>
    </>
  )
}
