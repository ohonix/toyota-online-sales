import { memo } from 'react'
import { getStatic } from '@lib/static'
import { animateScroll as scroll } from 'react-scroll'

import SpinIn from '@components/_common/SpinIn'
import Icon from './Icon'

function IconList() {
  return (
    <>
      <SpinIn>
        <div
          css={{
            img: {
              width: '2.5rem',
              height: '2.4rem',
            },
          }}>
          <Icon>
            <img src={getStatic('images/sticky-shortcut-icon/icon-line.svg')} />
          </Icon>
        </div>
      </SpinIn>
      <SpinIn delay={200}>
        <div
          css={{
            img: {
              width: '1.8rem',
              height: '1.1rem',
            },
          }}>
          <Icon onClick={() => scroll.scrollToTop()}>
            <img
              src={getStatic('images/sticky-shortcut-icon/icon-chevron-up.svg')}
            />
          </Icon>
        </div>
      </SpinIn>
    </>
  )
}

function areEqual(prevProps, nextProps) {
  return true
}
export default memo(IconList, areEqual)
