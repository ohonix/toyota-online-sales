import { useTheme } from '@emotion/react'
import { media } from '@lib/styles/helpers'
import { darken } from 'polished'

export default function IconWithLabel({ label, imgSrc }) {
  const { variables } = useTheme()
  return (
    <div
      css={{
        marginTop: '.5rem',
        minWidth: '15rem',
        width: '100%',
        height: '4.8rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '.5rem 1.5rem',
        boxShadow: '0 .3rem .6rem rgba(0,0,0,0.16)',
        backgroundColor: '#fff',
        color: '#000',
        borderRadius: '20rem',
        cursor: 'pointer',
        transition: 'all 0.3s',
        img: {
          display: 'block',
        },
        '&:active': {
          color: variables.colors.highlight.lvl1,
          backgroundColor: darken(0.1, '#fff'),
        },
        [media('xl')]: {
          '&:hover': {
            color: variables.colors.highlight.lvl1,
            backgroundColor: darken(0.1, '#fff'),
          },
        },
      }}>
      <div>{label}</div>
      <div>
        <img src={imgSrc} />
      </div>
    </div>
  )
}
