export default {
  about: '/about',
  'auth-login': '/member/login',
  'auth-dashboard': '/member/dashboard',
  'article-detail': '/article/[id]',
  customize: '/customize',
  home: '/',
  contact: '/contact',
  policy: '/policy',
  privilegeDetail: '/privilege/[id]',
  privilegeList: '/privilege',
}
