export default {
  sm: '576px',
  md: '768px',
  lg: '960px',
  xl: '1280px',
}
