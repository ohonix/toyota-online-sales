const zIndex = {
  header: 99,
  modal: 100,
  consent: 999,
  stickyShortcutIcons: 98,
}
const highlightColor = {
  lvl1: '#ED1B2F',
}
export default {
  light: {
    colors: {
      text: {
        primary: '#666',
        secondary: '#888',
      },
      background: {
        primary: '#fff',
        secondary: '#ddd',
      },
      link: '#444',
      highlight: highlightColor,
    },
    zIndex,
  },
  dark: {
    colors: {
      text: {
        primary: '#aaa',
        secondary: '#888',
      },
      background: {
        primary: '#111',
        secondary: '#888',
      },
      link: '#ddd',
      highlight: highlightColor,
    },
    zIndex,
  },
}
