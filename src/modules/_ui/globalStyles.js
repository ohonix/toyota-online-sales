import { css } from '@emotion/core'
import { media } from '@lib/styles/helpers'

export default function getGlobalStyles(variables) {
  return css`
    * {
      box-sizing: border-box;
      -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    }
    :root {
      font-size: 2.67vw;
      ${media('sm')} {
        font-size: 10px;
      }
    }
    html,
    body {
      margin: 0;
      font-family: 'db_helvethaica_x55_regular';
      font-size: 2.2rem;
      color: ${variables.colors.text.primary};
      background-color: #fbfbfd;
      line-height: 1.2;
    }
    img {
      max-width: 100%;
      height: auto;
    }
    a {
      color: ${variables.colors.link};
      word-break: break-all;
    }
    h1 {
      font-size: 4.8rem;
    }
    h2 {
      font-size: 4rem;
    }
    h3 {
      font-size: 3.8rem;
    }
    h4 {
      font-size: 3rem;
    }
    h5 {
      font-size: 2.8rem;
    }
    h6 {
      font-size: 2.3rem;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p {
      margin-bottom: 1rem;
    }
    a,
    button {
      outline: none;
      cursor: pointer;
    }

    .swiper-pagination-bullet-active {
      background-color: #000000;
    }

    .ReactModal__Overlay {
      z-index: ${variables.zIndex.modal};
      opacity: 0;
      transition: all 300ms ease-in-out;
    }

    .ReactModal__Overlay--after-open {
      opacity: 1;
    }
    .ReactModal__Overlay--before-close {
      opacity: 0;
    }

    .ReactModal__Content {
      max-width: 650px;
      margin: auto;
      width: 100%;
      opacity: 0;
      transform: translateY(100px);
      transition: all 300ms ease-in-out;
    }

    .ReactModal__Content--after-open {
      opacity: 1;
      transform: translateY(0px);
    }

    .ReactModal__Content--before-close {
      opacity: 0;
      transform: translateY(100px);
    }
  `
}
