import { Flex as ReflexBoxFlex } from 'reflexbox'
import { media } from '@lib/styles/helpers'
export { Box } from 'reflexbox'

export function Flex(props) {
  return <ReflexBoxFlex flexWrap="wrap" {...props} />
}

export function Container({ children, css }) {
  return (
    <div
      css={{
        paddingLeft: '2rem',
        paddingRight: '2rem',
        width: '100%',
        margin: 'auto',
        [media('sm')]: { maxWidth: '540px' },
        [media('md')]: { maxWidth: '720px' },
        [media('lg')]: { maxWidth: '960px' },
        [media('xl')]: { maxWidth: '1180px' },
        ...css,
      }}>
      {children}
    </div>
  )
}
