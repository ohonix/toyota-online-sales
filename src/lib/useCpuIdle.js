import { useEffect, useState } from 'react'
export default function useCpuIdle(callback = () => null) {
  const [cpuIdled, setCpuIdled] = useState(false)
  useEffect(() => {
    if ('requestIdleCallback' in window) {
      const handle = window.requestIdleCallback(() => {
        callback()
        setCpuIdled(true)
      })
      return () => {
        window.cancelIdleCallback(handle)
      }
    } else {
      callback()
      setCpuIdled(true)
    }
  }, [])
  return cpuIdled
}
